<?php

namespace WPML\Records;


use WPML\FP\Fns;
use WPML\FP\Obj;
use WPML\LIB\WP\WPDBMock;

class Test_Translations extends \OTGS_TestCase {
	use WPDBMock;
	use TranslationsMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
		$this->setUpTranslationRecords();
	}

	/**
	 * @test
	 */
	public function it_gets_for_post_type() {
		$elements = [
			[
				'element_id'           => 1,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 1,
				'status'               => null,
			],
			[
				'element_id'           => 2,
				'language_code'        => 'fr',
				'source_language_code' => 'en',
				'trid'                 => 1,
				'status'               => 1,
			],
			[
				'element_id'           => 3,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 2,
				'status'               => 10,
			],
		];

		foreach ( $elements as $element ) {
			$this->addTranslationRecordForPostType( 'post', $element );
		}

		$result = Translations::getForPostType(
			[ 'page' => Translations::OLDEST_FIRST, 'post' => Translations::NEWEST_FIRST ],
			'post'
		);

		$expected = Fns::map( Obj::assoc( 'element_type', 'post_post' ), $elements );
		$this->assertEquals( \wpml_collect( $expected ), $result );

		$result = Translations::getForPostType(
			[ 'page' => Translations::OLDEST_FIRST, 'post' => Translations::NEWEST_FIRST ],
			'page'
		);

		$this->assertEquals( \wpml_collect( [] ), $result );
	}

	/**
	 * @test
	 */
	public function it_gets_source_element_in_language() {
		$elements = [
			[
				'element_id'           => 1,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 1
			],
			[
				'element_id'           => 2,
				'language_code'        => 'fr',
				'source_language_code' => 'en',
				'trid'                 => 1
			],
			[
				'element_id'           => 3,
				'language_code'        => 'de',
				'source_language_code' => 'en',
				'trid'                 => 1
			],
			[
				'element_id'           => 4,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 2
			],
			[
				'element_id'           => 5,
				'language_code'        => 'fr',
				'source_language_code' => null,
				'trid'                 => 2
			],
		];

		$result = Translations::getSourceInLanguage( 'en', \wpml_collect( $elements ) );
		$this->assertEquals( \wpml_collect( [ $elements[0], $elements[3] ] ), $result );

		$result = Translations::getSourceInLanguage( 'fr', \wpml_collect( $elements ) );
		$this->assertEquals( \wpml_collect( [ $elements[4] ] ), $result );

		$result = Translations::getSourceInLanguage( 'es', \wpml_collect( $elements ) );
		$this->assertEquals( \wpml_collect( [] ), $result );
	}

	/**
	 * @test
	 */
	public function it_gets_source_element() {
		$elements = [
			[
				'element_id'           => 1,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 1
			],
			[
				'element_id'           => 2,
				'language_code'        => 'fr',
				'source_language_code' => 'en',
				'trid'                 => 1
			],
			[
				'element_id'           => 3,
				'language_code'        => 'de',
				'source_language_code' => 'en',
				'trid'                 => 1
			],
			[
				'element_id'           => 4,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 2
			],
			[
				'element_id'           => 5,
				'language_code'        => 'fr',
				'source_language_code' => null,
				'trid'                 => 2
			],
		];

		$result = Translations::getSource( \wpml_collect( $elements ) );
		$this->assertEquals( \wpml_collect( [ $elements[0], $elements[3], $elements[4] ] ), $result );
	}

	/**
	 * @test
	 */
	public function it_gets_translations_by_trid() {
		$elements = [
			[
				'element_id'           => 1,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 1,
				'element_type'  => 'post_page',
			],
			[
				'element_id'           => 2,
				'language_code'        => 'fr',
				'source_language_code' => 'en',
				'trid'                 => 1,
				'element_type'  => 'post_page',
			],
			[
				'element_id'           => 4,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 2,
				'element_type'  => 'post_page',
			],
			[
				'element_id'           => 3,
				'language_code'        => 'de',
				'source_language_code' => 'en',
				'trid'                 => 1,
				'element_type'  => 'post_page',
			],
		];

		foreach ( $elements as $element ) {
			$this->addTranslationRecord( $element );
		}

		$expected = [ $elements[0], $elements[1], $elements[3] ];
		$actual   = Translations::getByTrid( 1 );
		$this->assertEquals( $expected, $actual );

		$expected = [ $elements[2] ];
		$actual   = Translations::getByTrid( 2 );
		$this->assertEquals( $expected, $actual );

		$actual   = Translations::getByTrid( 3 );
		$this->assertEquals( [], $actual );
	}

	/**
	* @test
	*/
	public function it_gets_source_by_trid() {
		$elements = [
			[
				'element_id'           => 1,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 1,
				'element_type'         => 'post_page',
			],
			[
				'element_id'           => 2,
				'language_code'        => 'fr',
				'source_language_code' => 'en',
				'trid'                 => 1,
				'element_type'         => 'post_page',
			],
			[
				'element_id'           => 4,
				'language_code'        => 'en',
				'source_language_code' => null,
				'trid'                 => 2,
				'element_type'         => 'post_page',
			],
			[
				'element_id'           => 3,
				'language_code'        => 'de',
				'source_language_code' => 'en',
				'trid'                 => 1,
				'element_type'         => 'post_page',
			],
		];

		foreach ( $elements as $element ) {
			$this->addTranslationRecord( $element );
		}

		$this->assertEquals( $elements[0], Translations::getSourceByTrid( 1 ) );
	}
}