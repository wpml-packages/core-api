<?php

use tad\FunctionMocker\FunctionMocker;

define( 'MAIN_DIR', __DIR__ . '/..' );
define( 'WPML_PLUGIN_FOLDER', 'sitepress-multilingual-cms' );
define( 'WPML_TM_FOLDER', 'tm' );

require_once MAIN_DIR . '/vendor/antecedent/patchwork/Patchwork.php';

include MAIN_DIR . '/vendor/autoload.php';

require_once __DIR__ . '/mocks/MakeMock.php';
require_once __DIR__ . '/mocks/TranslationsMock.php';
require_once __DIR__ . '/mocks/LanguagesMock.php';
require_once __DIR__ . '/mocks/PostMock.php';
require_once __DIR__ . '/mocks/WPMLInstallationMock.php';
require_once __DIR__ . '/mocks/SettingsMock.php';
require_once __DIR__ . '/mocks/PostTypesMock.php';
require_once __DIR__ . '/mocks/TranslationsRepositoryMock.php';
require_once __DIR__ . '/mocks/TranslationServices/TranslationProxyMock.php';
require_once __DIR__ . '/mocks/Records/TranslationsMock.php';
require_once MAIN_DIR . '/vendor/wpml/wp/tests/mocks/WPDBMock.php';
require_once MAIN_DIR . '/vendor/wpml/wp/tests/mocks/PostMock.php';
require_once MAIN_DIR . '/vendor/wpml/wp/tests/mocks/PostTypeMock.php';
require_once MAIN_DIR . '/vendor/wpml/wp/tests/mocks/OptionMock.php';
require_once MAIN_DIR . '/vendor/wpml/wp/tests/mocks/OnActionMock.php';

FunctionMocker::init(
	[
		'whitelist' => [
			realpath( MAIN_DIR . '/classes' ),
		],
		'redefinable-internals' => [
			'function_exists',
			'time',
			'defined'
		],
	]
);
