<?php

namespace WPML\Media;

use WPML\LIB\WP\OptionMock;
use WPML\LIB\WP\PostMock;
use WPML\LIB\WP\WPDBMock;

class Test_Option extends \OTGS_TestCase {
	use WPDBMock;
	use OptionMock;
	use PostMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
		$this->setUpOptionMock();
		$this->setUpPostMock();
	}

	/**
	 * @test
	 */
	public function it_gets_new_content_settings() {
		// if the options are not set, it should return the default values
		$this->assertEquals( [
			'always_translate_media' => true,
			'duplicate_media'        => true,
			'duplicate_featured'     => true,
		], Option::getNewContentSettings() );

		// the returned values should be boolean
		update_option( '_wpml_media', [
			'new_content_settings' => [
				'always_translate_media' => '1',
				'duplicate_media'        => '',
				'duplicate_featured'     => 1,
			]
		] );

		$this->assertEquals( [
			'always_translate_media' => true,
			'duplicate_media'        => false,
			'duplicate_featured'     => true,
		], Option::getNewContentSettings() );
	}

	/**
	 * It sets new content settings.
	 * It casts the values to boolean.
	 * It additionally ignores any other settings that are not in the default list.
	 *
	 * @test
	 */
	public function it_sets_new_content_settings(  ) {
		Option::setNewContentSettings( [
			'always_translate_media' => '1',
			'duplicate_media'        => '',
			'duplicate_featured'     => 1,
			'other_setting'          => 'value',
		] );

		$this->assertEquals( [
			'always_translate_media' => true,
			'duplicate_media'        => false,
			'duplicate_featured'     => true,
		], Option::getNewContentSettings() );

		$this->assertEquals( [
			'new_content_settings' => [
				'always_translate_media' => true,
				'duplicate_media'        => false,
				'duplicate_featured'     => true,
			]
		], get_option( '_wpml_media' ) );
	}

	/**
	 *  1. It falls back to the default setting if the individual setting is not set.
	 *  2. If the second parameter $useGlobalSettings is set to false then it returns null if the individual setting is not set.
	 *  3. If individual setting is set, it returns it.
	 *
	 *  It uses "setDuplicateMediaForIndividualPost" and "setDuplicateFeaturedForIndividualPost" to set individual settings.
	 *
	 * @test
	 */
	public function it_check_if_media_should_be_duplicated_for_post() {

		// 1. It falls back to the default setting if the individual setting is not set.
		$this->assertEquals( true, Option::shouldDuplicateMedia( 1 ) );
		$this->assertEquals( true, Option::shouldDuplicateFeatured( 1 ) );

		// switch global settings to false
		Option::setNewContentSettings( [
			'always_translate_media' => true,
			'duplicate_media'        => false,
			'duplicate_featured'     => false,
		] );

		$this->assertEquals( false, Option::shouldDuplicateMedia( 1 ) );
		$this->assertEquals( false, Option::shouldDuplicateFeatured( 1 ) );

		// 2. If the second parameter $useGlobalSettings is set to false then it returns null if the individual setting is not set.
		$this->assertNull( Option::shouldDuplicateMedia( 1, false ) );
		$this->assertNull( Option::shouldDuplicateFeatured( 1, false ) );

		// 3. If individual setting is set, it returns it.
		Option::setNewContentSettings( [
			'always_translate_media' => true,
			'duplicate_media'        => true,
			'duplicate_featured'     => true,
		] );


		Option::setDuplicateMediaForIndividualPost( 1, false );
		Option::setDuplicateFeaturedForIndividualPost( 1, false );

		$this->assertEquals( false, Option::shouldDuplicateMedia( 1 ) );
		$this->assertEquals( false, Option::shouldDuplicateFeatured( 1 ) );
	}
}
