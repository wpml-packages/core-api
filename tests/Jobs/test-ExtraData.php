<?php

namespace WPML\Jobs;

class Test_ExtraData extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_decodes() {
		$extradata = '{&quot;id&quot;:&quot;field-test&quot;}';
		$expected  = [ 'id' => 'field-test' ];

		$this->assertEquals( $expected, ExtraData::decode( $extradata ) );
	}

	/**
	 * @test
	 */
	public function it_encodes() {
		$extradata = [ 'id' => 'field-test' ];
		$expected  = '{&quot;id&quot;:&quot;field-test&quot;}';

		\WP_Mock::userFunction( 'wp_json_encode', [
			'return' => function( $object ) {
				return json_encode( $object );
			}
		] );

		$this->assertEquals( $expected, ExtraData::encode( $extradata ) );
	}
}

