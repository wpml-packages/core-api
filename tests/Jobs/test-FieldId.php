<?php

namespace WPML\TM\Jobs;

class Test_FieldId extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_gets_term_meta_key() {
		$key    = 'my-meta-key';
		$termId = 123;

		$this->assertEquals( $key, FieldId::getTermMetaKey( FieldId::forTermMeta( $termId, $key ) ) );
	}

	/**
	 * @test
	 */
	public function it_gets_term_id() {
		$key    = 'my-meta-key';
		$termId = 123;

		$this->assertEquals( $termId, FieldId::get_term_id( FieldId::forTerm( $termId ) ) );
		$this->assertEquals( $termId, FieldId::get_term_id( FieldId::forTermDescription( $termId ) ) );
		$this->assertEquals( $termId, FieldId::get_term_id( FieldId::forTermMeta( $termId, $key ) ) );

	}

}

