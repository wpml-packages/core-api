<?php

namespace WPML\Settings;

use tad\FunctionMocker\FunctionMocker;

class TestLanguageNegotiation  extends \OTGS_TestCase {
	
	/**
	 * @test
	 * @dataProvider languagePerDirectoryDp
	 */
	public function testLanguagePerDirectory( $negotiationType, $expected ) {
		global $sitepress;
		
		$sitepress = $this->getSitepressMock( $negotiationType );
		FunctionMocker::replace( 'constant', 1 );
		
		$this->assertSame( $expected, LanguageNegotiation::isDir() );
	}

	public function languagePerDirectoryDp() {
		return [
			'languages in directories' => [ '1', true ],
			'languages in domains'     => [ '2', false ],
			'languages in URL param'   => [ '3', false ],
		];
	}

	/**
	 * @test
	 * @dataProvider languagePerDomainDp
	 */
	public function testLanguagePerDomain( $negotiationType, $expected ) {
		global $sitepress;
		
		$sitepress = $this->getSitepressMock( $negotiationType );
		FunctionMocker::replace( 'constant', 2 );
		
		$this->assertSame( $expected, LanguageNegotiation::isDomain() );
	}

	public function languagePerDomainDp() {
		return [
			'languages in directories' => [ '1', false ],
			'languages in domains'     => [ '2', true ],
			'languages in URL param'   => [ '3', false ],
		];
	}

	/**
	 * @test
	 * @dataProvider languagePerParamDp
	 */
	public function testLanguagePerParam( $negotiationType, $expected ) {
		global $sitepress;

		$sitepress = $this->getSitepressMock( $negotiationType );
		FunctionMocker::replace( 'constant', 3 );
		
		$this->assertSame( $expected, LanguageNegotiation::isParam() );
	}

	public function languagePerParamDp() {
		return [
			'languages in directories' => [ '1', false ],
			'languages in domains'     => [ '2', false ],
			'languages in URL param'   => [ '3', true ],
		];
	}

	private function getSitepressMock( $negotiationType ) {
		$sitepress = $this->getMockBuilder( 'SitePress')
			->disableOriginalConstructor()
			->setMethods( ['get_setting'] )
			->getMock();

		$sitepress->method( 'get_setting' )
			->with( 'language_negotiation_type' )
			->willReturn( $negotiationType );

		return $sitepress;
	}
}
