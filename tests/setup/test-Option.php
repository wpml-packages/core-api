<?php

namespace WPML\Element\API;

use tad\FunctionMocker\FunctionMocker;
use WPML\LIB\WP\OptionMock;
use WPML\LIB\WP\PostTypeMock;
use WPML\Setup\Option;

class Test_Option extends \OTGS_TestCase {
	use PostTypeMock;
	use OptionMock;

	public function setUp() {
		parent::setUp();
		$this->setUpOptionMock();
		$this->setUpPostTypeMock();
	}

	/**
	 * @test
	 */
	public function it_gets_default_translate_everything() {
		$this->assertEquals( true, Option::getTranslateEverythingDefaultInSetup( false ) );
		$this->assertEquals( false, Option::getTranslateEverythingDefaultInSetup( true ) );
	}

	/**
	 * @test
	 */
	public function it_sets_default_translation_mode() {
		Option::setDefaultTranslationMode( false );
		$this->assertEquals( ['myself'], Option::getTranslationMode() );
	}

	/**
	 * @test
	 */
	public function it_sets_default_translation_mode_when_preferred_service() {
		Option::setDefaultTranslationMode( true );
		$this->assertEquals( ['service'], Option::getTranslationMode() );
	}

	/**
	 * @test
	 */
	public function it_allows_to_control_translate_everything_state() {
		Option::setTMAllowed( true );
		Option::setTranslateEverything( true );
		$this->assertEquals( true, Option::shouldTranslateEverything() );
		$this->assertEquals( true, Option::getTranslateEverything() );

		Option::setTMAllowed( false );
		Option::setTranslateEverything( true );
		$this->assertEquals( false, Option::shouldTranslateEverything() );
		$this->assertEquals( false, Option::getTranslateEverything() );

		Option::setTMAllowed( true );
		Option::setTranslateEverything( false );
		$this->assertEquals( false, Option::shouldTranslateEverything() );
		$this->assertEquals( false, Option::getTranslateEverything() );
	}

}
