<?php

namespace WPML\Element\API;

use Mockery\MockInterface;
use WPML\FP\Fns;
use WPML\FP\Lst;
use WPML\FP\Obj;
use WPML\FP\Relation;
use WPML\FP\Wrapper;

trait TranslationsMock {

	private $translations;
	private $trid;

	public function setupElementTranslations() {
		global $sitepress;

		$this->translations = [];
		$this->trid         = 1;


		if ( ! $sitepress || ! $sitepress instanceof MockInterface ) {
			$sitepress = \Mockery::mock( '\SitePress' );
		}

		$sitepress->shouldReceive( 'set_element_language_details' )->andReturnUsing( function (
			$el_id,
			$el_type,
			$trid,
			$language_code,
			$src_language_code,
			$check_duplicates
		) {
			$trid = $trid ?: $this->trid ++;

			$this->translations[ $el_id . '-' . $el_type ] = (object) [
				'id'   => $el_id,
				'type' => $el_type,
				'trid' => $trid,
				'lang' => $language_code,
				'src'  => $src_language_code ? $src_language_code : $this->findSourceLang( $trid )
			];

			if ( method_exists( $this, 'setPostLang' ) ) {
				$this->setPostLang( $el_id, $language_code );
			}
		}
		);

		$sitepress->shouldReceive( 'get_element_trid' )->andReturnUsing( [ $this, 'findTrid' ] );

		$sitepress->shouldReceive( 'get_element_translations' )->andReturnUsing( function ( $trid, $type ) {
			return Wrapper::of( $this->translations )
			              ->map( Fns::filter( Relation::propEq( 'trid', $trid ) ) )
			              ->map( Fns::map( function ( $t ) {
				              return (object) [
					              'original'             => is_null( $t->src ) || $t->src === $t->lang,
					              'element_id'           => $t->id,
					              'source_language_code' => $t->src,
					              'language_code'        => $t->lang,
					              'trid'                 => $t->trid,
				              ];
			              } ) )
			              ->map( Lst::keyBy('language_code') )
			              ->get();
		} );

	}

	public function findTrid( $id, $type ) {
		return Obj::pathOr( null, [ $id . '-' . $type, 'trid' ], $this->translations );
	}

	public function findSourceLang( $trid ) {
		return Wrapper::of( $this->translations )
		              ->map( Fns::filter( Relation::propEq( 'trid', $trid ) ) )
		              ->map( Fns::reduce( function ( $c, $t ) { return is_null( $t->src ) ? $t->lang : $c; }, null ) )
		              ->get();

	}

}
