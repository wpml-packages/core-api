<?php

namespace WPML\API;

use WPML\FP\Obj;

trait SettingsMock {

	public function setUpSettingsMock() {
		global $sitepress;

		$sitepressSettings = [];

		if ( ! $sitepress ) {
			$sitepress = \Mockery::mock( 'SitePress' );
		}

		$sitepress->shouldReceive( 'get_setting' )->andReturnUsing(
			function ( $key, $default = false ) use ( &$sitepressSettings ) {
				return Obj::propOr( $default, $key, $sitepressSettings );
			}
		);

		$sitepress->shouldReceive( 'set_setting' )->andReturnUsing(
			function ( $key, $value, $save ) use ( &$sitepressSettings ) {
				$sitepressSettings[ $key ] = $value;
			}
		);


	}

}
