<?php

namespace WPML\Records;


use WPML\FP\Fns;
use WPML\FP\Logic;
use WPML\FP\Relation;
use WPML\FP\Str;
use function WPML\FP\pipe;

trait TranslationsMock {

	private $iclTranslationsRecords;

	public function setUpTranslationRecords() {
		$this->iclTranslationsRecords = [];

		$this->addPrepareHandler(
			Logic::allPass( [
				pipe( $this->getQuery(), Str::includes( 'SELECT translations.element_id, translations.language_code, translations.source_language_code, translations.trid, translation_status.status' ) ),
				pipe( $this->getQuery(), Str::includes( 'FROM wp_icl_translations' ) ),
			] ),
			function ( $sql, $postType ) {
				return Fns::filter( Relation::propEq( 'element_type', $postType ), $this->iclTranslationsRecords );
			}
		);

		$this->addPrepareHandler(
			pipe( $this->getTrimmedQuery(), Str::startsWith( "SELECT * FROM wp_icl_translations WHERE trid =" ) ),
			function ( $query, $trid ) {
				return Fns::filter(Relation::propEq('trid', $trid), $this->iclTranslationsRecords);
			}
		);
	}

	/**
	 * @param string $postType eg. page, post, cars etc
	 * @param array $record [element_id, language_code, source_language_code, trid, status]
	 */
	public function addTranslationRecordForPostType( $postType, array $record ) {
		$record['element_type']           = 'post_' . $postType;
		$this->iclTranslationsRecords[] = $record;
	}

	public function addTranslationRecord( array $record ) {
		$this->iclTranslationsRecords[] = $record;
	}
}