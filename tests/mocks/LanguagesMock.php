<?php

namespace WPML\Element\API;

use WPML\FP\Fns;
use WPML\FP\Logic;
use WPML\FP\Lst;
use WPML\FP\Obj;
use WPML\FP\Relation;
use WPML\FP\Str;
use function WPML\FP\pipe;

trait LanguagesMock {
	/**
	 * We append fields: display_name dynamically in the method getActiveLanguages.
	 *
	 * @var array[]
	 */
	protected $availableLanguages = [
		'en' => [
			'code'           => 'en',
			'id'             => 1,
			'english_name'   => 'English',
			'native_name'    => 'English',
			'display_name'   => 'English',
			'major'          => 1,
			'default_locale' => 'en_US',
			'tag'            => 'en',
			'encode_url'     => 0,
		],
		'de' => [
			'code'           => 'de',
			'id'             => 2,
			'english_name'   => 'German',
			'native_name'    => 'Deutsch',
			'display_name'   => 'German',
			'major'          => 1,
			'default_locale' => 'de_DE',
			'tag'            => 'de',
			'encode_url'     => 0,
		],
		'fr' => [
			'code'           => 'fr',
			'id'             => 3,
			'english_name'   => 'French',
			'native_name'    => 'Français',
			'display_name'   => 'French',
			'major'          => 1,
			'default_locale' => 'fr_FR',
			'tag'            => 'fr',
			'encode_url'     => 0,
		],
		'es' => [
			'code'           => 'es',
			'id'             => 4,
			'english_name'   => 'Spanish',
			'native_name'    => 'Español',
			'display_name'   => 'Spanish',
			'major'          => 1,
			'default_locale' => 'es_ES',
			'tag'            => 'es',
			'encode_url'     => 0,
		],
		'ru' => [
			'code'           => 'ru',
			'id'             => 5,
			'english_name'   => 'Russian',
			'native_name'    => 'русский',
			'display_name'   => 'Russian',
			'major'          => 1,
			'default_locale' => 'ru_RU',
			'tag'            => 'ru',
			'encode_url'     => 0,
		],
		'pl' => [
			'code'           => 'pl',
			'id'             => 6,
			'english_name'   => 'Polish',
			'native_name'    => 'polski',
			'major'          => 0,
			'default_locale' => 'pl_PL',
			'display_name'   => 'Polish',
			'tag'            => 'pl',
			'encode_url'     => 0,
		],
		'he' => [
			'code'           => 'he',
			'id'             => 20,
			'english_name'   => 'Hebrew',
			'native_name'    => "\u05e2\u05d1\u05e8\u05d9\u05ea",
			'display_name'   => 'Hebrew',
			'major'          => 0,
			'default_locale' => 'he_IL',
			'tag'            => 'he',
			'encode_url'     => 0,
		],
		'zu' => [
			'code'           => 'zu',
			'id'             => 60,
			'english_name'   => 'Zulu',
			'native_name'    => "",
			'display_name'   => 'Zulu',
			'major'          => 0,
			'default_locale' => '',
			'tag'            => 'zu',
			'encode_url'     => 0,
		],
		'pt-br' => [
			'code'           => 'pt-br',
			'id'             => 42,
			'english_name'   => 'Portuguese, Brazil',
			'native_name'    => "Português",
			'display_name'   => 'Portuguese, Brazil',
			'major'          => 0,
			'default_locale' => 'pt_BR',
			'tag'            => 'pt-br',
			'encode_url'     => 0,
		],
		'pt-pt' => [
			'code'           => 'pt-pt',
			'id'             => 42,
			'english_name'   => 'Portuguese, Portugal',
			'native_name'    => "Português",
			'display_name'   => 'Portuguese, Portugal',
			'major'          => 0,
			'default_locale' => 'pt_PT',
			'tag'            => 'pt-pt',
			'encode_url'     => 0,
		],
	];

	private $languageNamesTranslations = [
		'en' => [
			'en' => 'English',
			'de' => 'German',
			'fr' => 'French',
			'es' => 'Spanish',
			'ru' => 'Russian',
			'pl' => 'Polish',
			'he' => 'Hebrew',
			'zu' => 'Zulu',
		],
		'de' => [
			'en' => 'Englisch',
			'de' => 'Deutsch',
			'fr' => 'Französisch',
			'es' => 'Spanisch',
			'ru' => 'Russisch',
			'pl' => 'Polnisch',
			'he' => 'Hebräisch',
			'zu' => '',
		],
		'fr' => [
			'en' => 'Anglais',
			'de' => 'Allemand',
			'fr' => 'Français',
			'es' => 'Espagnol',
			'ru' => 'Russe',
			'pl' => 'Polonais',
			'he' => 'Hébreu',
			'zu' => '',
		],
		'es' => [
			'en' => 'Inglés',
			'de' => 'Alemán',
			'fr' => 'Francés',
			'es' => 'Español',
			'ru' => 'Ruso',
			'pl' => 'Polaco',
			'he' => 'Hebreo',
			'zu' => '',
		],
		'ru' => [
			'en' => 'Английский',
			'de' => 'Немецкий',
			'fr' => 'Французский',
			'es' => 'Испанский',
			'ru' => 'Русский',
			'pl' => 'Польский',
			'he' => 'Иврит',
			'zu' => '',
		],
		'pl' => [
			'en'    => 'Angielski',
			'de'    => 'Niemiecki',
			'fr'    => 'Francuski',
			'es'    => 'Hiszpański',
			'ru'    => 'Rosyjski',
			'pl'    => 'Polski',
			'he'    => 'Hebrajski',
			'zu'    => '',
			'pt-pt' => 'Portugalski, Portugalia',
			'pt-br' => 'Portugalski, Brazylia'
		],
		'he' => [
			'en' => 'אנגלית',
			'de' => 'גרמנית',
			'fr' => 'צרפתית',
			'es' => 'ספרדית',
			'ru' => 'רוסית',
			'pl' => 'פולנית',
			'he' => 'עברית',
			'zu' => '',
		]
	];

	private $languageTranslations = [];

	protected $activeLanguages;

	protected $defaultLanguage;

	protected $currentLanguage;

	protected $languageTransitions = [];

	protected $flags = [
		'en' => [ 'flag' => 'http://site.com/en.png', 'from_template' => 0 ],
		'de' => [ 'flag' => 'http://site.com/de.png', 'from_template' => 0 ],
		'fr' => [ 'flag' => 'http://site.com/fr.png', 'from_template' => 0 ],
	];

	protected $rtl = [
		'en' => false,
		'de' => false,
		'fr' => false,
		'he' => true,
	];

	protected $wpLocalesMap = [];

	public function setupLanguagesMock() {
		global $sitepress;

		if ( ! $sitepress ) {
			$sitepress = \Mockery::mock( 'SitePress' );
		}

		$sitepress->shouldReceive( 'get_active_languages' )->andReturnUsing( [ $this, 'getActiveLanguages' ] );

		$sitepress->shouldReceive( 'get_flag_url' )->andReturnUsing( function ( $code ) {
			return Obj::path( [ $code, 'flag' ], $this->flags );
		} );

		$sitepress->shouldReceive( 'get_flag' )->andReturnUsing( function ( $code ) {
			return Obj::prop( $code, $this->flags );
		} );

		$sitepress->shouldReceive( 'get_languages' )->andReturnUsing( function ( $userLang = false ) {
			return $userLang ? Fns::map( function ( $language ) use ( $userLang ) {
				$language['display_name'] = Obj::pathOr( $language['display_name'], [
					$userLang,
					$language['code']
				], $this->languageNamesTranslations );

				return $language;
			}, $this->availableLanguages ) : $this->availableLanguages;
		} );
		$sitepress->shouldReceive( 'is_rtl' )->andReturnUsing( Obj::prop( Fns::__, $this->rtl ) );

		$sitepress->shouldReceive( 'get_default_language' )->andReturnUsing( function () {
			return $this->defaultLanguage;
		} );
		$sitepress->shouldReceive( 'get_current_language' )->andReturnUsing( function () {
			return $this->currentLanguage;
		} );
		$sitepress->shouldReceive( 'get_language_details' )->andReturnUsing( [ $this, 'getLanguageDetails' ] );


		$this->addInsertHandler(
			$this->tableEquals('wp_icl_languages'),
			function($table, $data) {
				global $wpdb;

				$data['id']                                               = count( $this->availableLanguages ) + 1;
				$this->availableLanguages[ $data['code'] ]                = $data;
				$this->availableLanguages[ $data['code'] ]['native_name'] = $data['english_name'];

				$wpdb->insert_id = $data['id'];

				return 1;
			}
		);

		$this->addQueryHandler(
			pipe( $this->getQuery(), Str::startsWith( "REPLACE INTO wp_icl_languages_translations" ) ),
			function ( $query ) {
				return Obj::prop( 'id', Lst::last( $this->languageTranslations ) );
			}
		);

		$this->addPrepareHandler(
			pipe( $this->getTrimmedQuery(), Str::startsWith( "REPLACE INTO wp_icl_languages_translations" ) ),
			function ( $query, $langCode, $displayLangCode, $name ) {
				$this->languageTranslations[] = [
					'id'                    => count( $this->languageTranslations ) + 1,
					'language_code'         => $langCode,
					'display_language_code' => $displayLangCode,
					'name'                  => $name,
				];

				return $query;
			}
		);

		$this->addPrepareHandler(
			pipe( $this->getQuery(), Logic::allPass( [
				Str::includes( 'SELECT language_code' ),
				Str::includes( 'FROM wp_icl_languages_translations' ),
				Str::includes( 'WHERE name =' ),
				Str::includes( 'AND display_language_code =' ),
			] ) ),
			function ( $sql, $langName, $currentLangCode ) {
				$translations = $this->languageNamesTranslations[ $currentLangCode ];
				foreach ( $translations as $code => $name ) {
					if ( $name === $langName ) {
						return $code;
					}
				}

				return null;
			}
		);

		Languages::init(); // Make sure we use this instance of $sitepress

		$this->activeLanguages = [ 'en', 'de', 'fr' ];

		\WP_Mock::userFunction( 'wpml_reload_active_languages_setting', [
			'return' => function () {
				return $this->activeLanguages;
			}
		] );

		\WP_Mock::userFunction( 'wp_download_language_pack', [
			'return' => function ( $code ) {
				return Obj::propOr( false, $code, $this->wpLocalesMap );
			}
		] );

		\WP_Mock::userFunction( 'wpml_reload_active_languages_setting', [
			'return' => function () {
				return $this->activeLanguages;
			}
		] );

		\WP_Mock::userFunction( 'wp_download_language_pack', [
			'return' => function ( $code ) {
				return Obj::propOr( false, $code, $this->wpLocalesMap );
			}
		] );

		\WP_Mock::userFunction( 'icl_get_languages_codes', [
			'return' => function () {
				return array_combine( Lst::pluck( 'display_name', $this->availableLanguages ), Lst::pluck( 'code', $this->availableLanguages ) );
			}
		] );

		$sitepress->shouldReceive( 'get_current_language' )->andReturn( $this->currentLanguage );
		$sitepress->shouldReceive( 'switch_lang' )->andReturnUsing( function ( $lang ) {
			$this->languageTransitions[] = [ $this->currentLanguage, $lang ];
			$this->currentLanguage       = $lang;
		} );
	}

	public function tearDownLanguagesMock() {
		global $sitepress, $wpdb;

		$sitepress = $wpdb = null;
	}


	public function getActiveLanguages() {
		$map = function ( $language ) {
			$language['display_name'] = $language['english_name'];

			return $language;
		};

		$isActive = pipe( Obj::prop( 'code' ), Lst::includes( Fns::__, $this->activeLanguages ) );

		return \wpml_collect( $this->availableLanguages )->filter( $isActive )->map( $map )->toArray();
	}

	public function setActiveLanguages( array $languages ) {
		$allowed    = array_keys( $this->availableLanguages );
		$notAllowed = array_diff( $languages, $allowed );
		if ( $notAllowed ) {
			throw new \InvalidArgumentException(
				'Languages %s are not allowed. Allowed values: %s.',
				implode( ', ', $notAllowed ),
				implode( ', ', $allowed )
			);
		}

		$this->activeLanguages = $languages;
	}

	public function setDefaultLanguage( $languageCode ) {
		$allowed = array_keys( $this->availableLanguages );
		if ( ! in_array( $languageCode, $allowed ) ) {
			throw new \InvalidArgumentException(
				'Language %s is not allowed. Allowed values: %s.',
				$languageCode,
				implode( ', ', $allowed )
			);
		}

		$this->defaultLanguage = $languageCode;
	}

	public function setCurrentLanguage( $languageCode ) {
		$allowed = array_keys( $this->availableLanguages );
		if ( ! in_array( $languageCode, $allowed ) ) {
			throw new \InvalidArgumentException(
				'Language %s is not allowed. Allowed values: %s.',
				$languageCode,
				implode( ', ', $allowed )
			);
		}

		$this->currentLanguage = $languageCode;
	}

	public function getLanguageDetails( $languageCode ) {
		return isset( $this->availableLanguages[ $languageCode ] ) ?
			$this->availableLanguages[ $languageCode ] + [ 'built_in' => true ] :
			false;
	}

	public function addWPLocaleMapping( $code, $wpLocale ) {
		$this->wpLocalesMap[ $code ] = $wpLocale;
	}

	public function assertDidSwitchLanguages( ...$transitions ) {
		$this->assertEquals( count( $this->languageTransitions ), count( $transitions ) - 1 );
		for ( $i = 0; $i < count( $transitions ) - 1; $i ++ ) {
			$this->assertEquals( [ $transitions[ $i ], $transitions[ $i + 1 ] ], $this->languageTransitions[ $i ] );

		}
	}
}

