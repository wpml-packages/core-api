<?php

class TranslationProxy {
	public static $mock_default_suid = false;

	public static function get_tp_default_suid() {
		return static::$mock_default_suid;
	}
}
