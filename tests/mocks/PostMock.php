<?php

namespace WPML\API;

use WPML\FP\Obj;

trait PostMock {

	private $postLangs = [];

	protected function setUpPostMock() {
		global $wpml_post_translations;
		$wpml_post_translations = \Mockery::mock( '\WPML_Admin_Post_Actions' );

		$wpml_post_translations->shouldReceive( 'get_element_lang_code' )->andReturnUsing( function ( $postId ) {
			return Obj::propOr( 'en', $postId, $this->postLangs );
		} );
	}

	protected function setPostLang( $postId, $langCode ) {
		$this->postLangs[ $postId ] = $langCode;
	}
}