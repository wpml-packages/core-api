<?php

namespace WPML\API;

use WPML\FP\Fns;
use WPML\FP\Obj;

trait PostTypesMock {
	/** @var array  */
	private $customPostTypes = [];

	/** @var array */
	private $translatable;

	/** @var array */
	private $displayAsTranslatable;

	protected function setUpPostTypesMock() {
		$this->translatable          = [ 'page', 'post' ];
		$this->displayAsTranslatable = [];

		global $sitepress;
		if ( ! $sitepress ) {
			$sitepress = \Mockery::mock( '\SitePress' );
		}

		$sitepress->shouldReceive( 'get_translatable_documents' )->andReturnUsing( function () {
			return wpml_collect( $this->translatable )
				->mapWithKeys( Fns::converge( Obj::objOf(), [ Fns::identity(), Obj::prop( Fns::__, $this->getPostTypes() ) ] ) )
				->toArray();
		} );

		$sitepress->shouldReceive( 'get_display_as_translated_documents' )->andReturnUsing( function () {
			return wpml_collect( $this->displayAsTranslatable )
				->mapWithKeys( Obj::objOf( Fns::__, 1 ) )
				->toArray();
		} );
	}

	protected function setTranslatablePostTypes( array $postTypes ) {
		$this->translatable = $postTypes;
	}

	/**
	 * @param string $postType
	 *
	 * @return void
	 */
	protected function addTranslatablePostType( $postType ) {
		$this->translatable[] = $postType;
	}

	protected function setDisplayAsTranslatablePostTypes( array $postTypes ) {
		$this->displayAsTranslatable = $postTypes;
	}

	protected function addPostTypeDefinition( $name, $pluralLabel, $singularLabel ) {
		$this->customPostTypes[ $name ] = (object) [
			'name'   => $name,
			'label'  => $pluralLabel,
			'labels' => [
				'name'          => $pluralLabel,
				'singular_name' => $singularLabel,
			]
		];
	}

	protected function getPostTypes() {
		return array_merge(
			[
				'post'       => (object) [
					'name'   => 'post',
					'label'  => 'Posts',
					'labels' => [
						'name'          => 'Posts',
						'singular_name' => 'Post',
					]
				],
				'page'       => (object) [
					'name'   => 'page',
					'label'  => 'Pages',
					'labels' => [
						'name'          => 'Pages',
						'singular_name' => 'Page',
					]
				],
				'attachment' => (object) [
					'name'   => 'attachment',
					'label'  => 'Media',
					'labels' => [
						'name'          => 'Media',
						'singular_name' => 'Media',
					]
				],
			],
			$this->customPostTypes
		);
	}
}
