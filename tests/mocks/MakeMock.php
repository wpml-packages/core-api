<?php

namespace WPML\API;

use WPML\FP\Obj;

trait MakeMock {

	private $instances;

	public function setUpMakeMock() {
		$this->instances = [];

		\WP_Mock::userFunction( 'WPML\Container\make', [
			'return' => function ( $className ) {
				return Obj::prop( $className, $this->instances );
			}
		] );
	}

	public function mockMake( $className ) {
		$this->assertFalse(
			isset( $this->instances[ $className ] ),
			"MakeMock can only mock the same class once. You'll have to mock manually"
		);

		$this->instances[ $className ] = \Mockery::mock( $className );

		return $this->instances[ $className ];
	}
}

