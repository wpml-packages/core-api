<?php

namespace WPML\API;

trait WPMLInstallationMock {

	private $firstInstallationVersion;

	public function setupInstallationMock() {
		$this->firstInstallationVersion = '0.0.0';

		\Mockery::mock( 'overload:\WPML_Installation' )
		        ->shouldReceive( 'getStartVersion' )
		        ->andReturnUsing( function () {
			        return $this->firstInstallationVersion;
		        } );
	}

	public function setFirstInstallationWPMLVersion( $version ) {
		$this->firstInstallationVersion = $version;
	}
}