<?php

namespace mocks;

use WPML\FP\Logic;
use WPML\FP\Lst;
use WPML\FP\Str;
use function WPML\FP\pipe;

trait TranslationsRepositoryMock {

	private $recordsToPreloadByPosts = [];

	public function setUpTranslationsRepository() {
		$this->addPrepareHandler(
			Logic::allPass( [
				pipe( $this->getQuery(), Str::includes( 'SELECT translations.translation_id' ) ),
				pipe( $this->getQuery(), Str::includes( 'FROM wp_icl_translations as translations' ) ),
				pipe( $this->getQuery(), Str::includes( 'WHERE translations.trid IN' ) ),
			] ),
			function ( $sql ) {
				return Lst::flattenToDepth( 2, $this->recordsToPreloadByPosts );
			}
		);
	}

	public function addPostTranslationRecordsToPreload( $postId, $postType, $records ) {
		$this->recordsToPreloadByPosts[ $postId ][ $postType ] = $records;
	}
}