<?php

namespace WPML\Core\BackgroundTask\Command;

use WPML\API\MakeMock;
use WPML\LIB\WP\WPDBMock;
use WPML\Core\BackgroundTask\Model\BackgroundTask as BackgroundTaskModel;

class Test_PersistBackgroundTask extends \OTGS_TestCase {
	use MakeMock;
	use WPDBMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
	}

	/**
	 * @test
	 */
	public function it_runs() {
		global $wpdb;
		$wpdb->insert_id = 0;
		$expected = [
			'task_type'       => 'endpointClass',
			'task_status'     => 0,
			'starting_date'   => null,
			'total_count'     => 0,
			'completed_count' => 0,
			'completed_ids'   => null,
			'payload'         => serialize( [ 'test' => 'test' ] ),
			'retry_count'     => 0,
		];

		$this->addInsertHandler(
			function( $table, $data ) use ( $wpdb ) {
				return $table === $wpdb->prefix . 'icl_background_task' && 'endpointClass' === $data['task_type'];
			},
			function( $table, $data ) use ( $expected ) {
				$this->assertEquals( $expected, $data );
			}
		);

		$persistBackgroundTask = new PersistBackgroundTask( $wpdb );
		$backgroundTask = $persistBackgroundTask->run(
			'endpointClass',
			BackgroundTaskModel::TASK_STATUS_PENDING,
			0,
			[ 'test' => 'test' ],
			[]
		);

		$this->assertEquals( 0, $backgroundTask->getTaskId() );
	}
}
