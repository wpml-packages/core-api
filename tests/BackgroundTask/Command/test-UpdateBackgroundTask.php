<?php

namespace WPML\Core\BackgroundTask\Command;

use WPML\API\MakeMock;
use WPML\LIB\WP\WPDBMock;
use WPML\Core\BackgroundTask\Command\UpdateBackgroundTask;
use WPML\Core\BackgroundTask\Model\BackgroundTask as BackgroundTaskModel;
use WPML\Core\BackgroundTask\Exception\TaskNotRunnable\ExceededMaxRetriesException;
use WPML\Core\BackgroundTask\Exception\TaskNotRunnable\TaskIsCompletedException;
use WPML\Core\BackgroundTask\Exception\TaskNotRunnable\TaskIsPausedException;
use WPML\Core\BackgroundTask\Model\TaskEndpointInterface;
use WPML\Collect\Support\Collection;

class EndpointMock implements TaskEndpointInterface {
	public function isDisplayed() { }

	public function isValidTask( $taskId ) { }

	public function getType() { }

	public function getMaxRetries() { }

	public function getLockTime() { }

	public function getTotalRecords( Collection $data ) { }

	public function getDescription( Collection $data ) { }

	public function run( Collection $data ) { }
}

class Test_UpdateBackgroundTask extends \OTGS_TestCase {
	use MakeMock;
	use WPDBMock;

	private $backgroundTask;

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
		$this->backgroundTask = $this->make_backgroundtask();
	}

	private function make_backgroundtask( $data = array() ) {
		$task = new BackgroundTaskModel();
		$task->setTaskId( 1 );
		if ( array_key_exists( 'payload', $data ) ) {
			$task->setPayload( $data['payload'] );
		}
		$task->setTaskType( EndpointMock::class );

		return $task;
	}

	/**
	 * @test
	 */
	public function it_not_starts_task_when_task_is_paused() {
		global $wpdb;

		$this->backgroundTask->setStatus( BackgroundTaskModel::TASK_STATUS_PAUSED );
		$this->expectException( TaskIsPausedException::class );

		$endpointMock = $this->mockMake( EndpointMock::class );
		$endpointMock->shouldReceive( 'getType' )->andReturn( EndpointMock::class );

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$updateBackgroundTask->startTask( $this->backgroundTask,  $endpointMock );
	}

	/**
	 * @test
	 */
	public function it_not_starts_task_when_task_is_completed() {
		global $wpdb;

		$this->backgroundTask->setStatus( BackgroundTaskModel::TASK_STATUS_COMPLETED );
		$this->expectException( TaskIsCompletedException::class );

		$endpointMock = $this->mockMake( EndpointMock::class );
		$endpointMock->shouldReceive( 'getType' )->andReturn( EndpointMock::class );

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$updateBackgroundTask->startTask( $this->backgroundTask,  $endpointMock );
	}

	/**
	 * @test
	 */
	public function it_starts_task_when_task_is_pending() {
		global $wpdb;

		$this->backgroundTask->setStatus( BackgroundTaskModel::TASK_STATUS_PENDING );

		$endpointMock = $this->mockMake( EndpointMock::class );
		$endpointMock->shouldReceive( 'getType' )->andReturn( EndpointMock::class );

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$backgroundTask = $updateBackgroundTask->startTask( $this->backgroundTask,  $endpointMock );

		$this->assertInstanceOf( \DateTime::class, $backgroundTask->getStartingDate() );
		$this->assertEquals( BackgroundTaskModel::TASK_STATUS_INPROGRESS, $backgroundTask->getStatus() );
	}

	/**
	 * @test
	 */
	public function it_pauses_task_when_task_has_no_retries_left() {
		global $wpdb;
		$this->expectException( ExceededMaxRetriesException::class );

		$this->backgroundTask->setStatus( BackgroundTaskModel::TASK_STATUS_INPROGRESS );
		$this->backgroundTask->setRetryCount( 1 );

		$endpointMock = $this->mockMake( EndpointMock::class );
		$endpointMock->shouldReceive( 'getMaxRetries' )->andReturn( 1 );

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$backgroundTask = $updateBackgroundTask->startTask( $this->backgroundTask,  $endpointMock );

		$this->assertEquals( BackgroundTaskModel::TASK_STATUS_PAUSED, $backgroundTask->getStatus() );
	}

	/**
	 * @test
	 */
	public function it_retries_when_is_inprogress() {
		global $wpdb;

		$this->backgroundTask->setStatus( BackgroundTaskModel::TASK_STATUS_INPROGRESS );
		$this->backgroundTask->setRetryCount( 0 );

		$endpointMock = $this->mockMake( EndpointMock::class );
		$endpointMock->shouldReceive( 'getMaxRetries' )->andReturn( 1 );

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$backgroundTask = $updateBackgroundTask->startTask( $this->backgroundTask,  $endpointMock );

		$this->assertEquals( 1, $backgroundTask->getRetryCount() );
	}

	/**
	 * @test
	 */
	public function it_updates() {
		global $wpdb;
		$wpdb->insert_id = 0;
		$this->backgroundTask->setTaskType( EndpointMock::class );
		$this->backgroundTask->setPayload( ['test' => 'test'] );

		$expected = [
			'task_type' => EndpointMock::class,
			'task_status' => 0,
			'starting_date' => null,
			'total_count' => 0,
			'completed_count' => 0,
			'completed_ids' => null,
			'payload' => serialize( ['test' => 'test' ] ),
			'retry_count' => 0,
		];

		$this->addUpdateHandler(
			function ($table, $data) use ($wpdb) {
				return $table === $wpdb->prefix . 'icl_background_task';
			},
			function ($table, $data) use ($expected) {
				$this->assertEquals($expected, $data);
			}
		);

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$updateBackgroundTask->runUpdate( $this->backgroundTask );
	}

	/**
	 * @test
	 */
	public function it_stops() {
		global $wpdb;

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$updateBackgroundTask->runStop( $this->backgroundTask );

		$this->assertEquals(BackgroundTaskModel::TASK_STATUS_PAUSED, $this->backgroundTask->getStatus() );
		$this->assertEquals( 0, $this->backgroundTask->getCompletedCount() );
		$this->assertCount( 0, $this->backgroundTask->getCompletedIds() );
	}

	/**
	 * @test
	 */
	public function it_saves_status_resumed() {
		global $wpdb;

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$updateBackgroundTask->saveStatusResumed( $this->backgroundTask );

		$this->assertEquals(BackgroundTaskModel::TASK_STATUS_PENDING, $this->backgroundTask->getStatus() );
	}

	/**
	 * @test
	 */
	public function it_saves_status_restart() {
		global $wpdb;

		$updateBackgroundTask = new UpdateBackgroundTask( $wpdb );
		$updateBackgroundTask->saveStatusRestart( $this->backgroundTask );

		$this->assertEquals(BackgroundTaskModel::TASK_STATUS_PENDING, $this->backgroundTask->getStatus() );
		$this->assertEquals( 0, $this->backgroundTask->getCompletedCount() );
		$this->assertCount( 0, $this->backgroundTask->getCompletedIds() );
	}
}
