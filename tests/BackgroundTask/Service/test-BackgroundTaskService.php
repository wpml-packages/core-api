<?php

namespace WPML\Core\BackgroundTask\Service;

use WPML\API\MakeMock;
use WPML\Collect\Support\Collection;
use WPML\Core\BackgroundTask\Command\DeleteBackgroundTask;
use WPML\Core\BackgroundTask\Model\TaskEndpointInterface;
use WPML\TM\AutomaticTranslation\Actions\Actions;
use WPML\Core\BackgroundTask\Model\BackgroundTask as BackgroundTaskModel;
use WPML\Core\BackgroundTask\Command\PersistBackgroundTask;
use WPML\Core\BackgroundTask\Command\UpdateBackgroundTask;
use WPML\Core\BackgroundTask\Repository\BackgroundTaskRepository;
use WPML\Core\BackgroundTask\Exception\TaskIsNotRunnableException;
use WPML\LIB\WP\WPDBMock;

class EndpointMock implements TaskEndpointInterface {
	public function isDisplayed() { }

	public function isValidTask( $taskId ) { }

	public function getType() { }

	public function getMaxRetries() { }

	public function getLockTime() { }

	public function getTotalRecords( Collection $data ) { }

	public function getDescription( Collection $data ) { }

	public function run( Collection $data ) { }
}

class WrongEndpointMock { }

class Test_BackgroundTaskService extends \OTGS_TestCase {
	use MakeMock;
	use WPDBMock;

	private $backgroundTask;

	private $backgroundTaskRepositoryMock;
	private $persistBackgroundTaskMock;
	private $updateBackgroundTaskMock;
	private $deleteBackgroundTaskMock;

	private $backgroundTaskService;

	public function setUp() {
		parent::setUp();
		$this->setUpWPDBMock();
		$this->setUpMakeMock();

		$this->backgroundTask               = $this->make_backgroundtask();
		$this->deleteBackgroundTaskMock     = $this->mockMake( DeleteBackgroundTask::class );
		$this->backgroundTaskRepositoryMock = $this->mockMake( BackgroundTaskRepository::class );
		$this->persistBackgroundTaskMock    = $this->mockMake( PersistBackgroundTask::class );
		$this->updateBackgroundTaskMock     = $this->mockMake( UpdateBackgroundTask::class );

		$this->backgroundTaskService = new BackgroundTaskService(
			$this->backgroundTaskRepositoryMock,
			$this->persistBackgroundTaskMock,
			$this->updateBackgroundTaskMock,
			$this->deleteBackgroundTaskMock
		);
	}

	private function make_backgroundtask( $data = array() ) {
		$task = new BackgroundTaskModel();
		$task->setTaskId( 1 );
		if ( array_key_exists( 'payload', $data ) ) {
			$task->setPayload( $data['payload'] );
		}
		$task->setTaskType( EndpointMock::class );

		return $task;
	}

	/**
	 * @test
	 */
	public function it_starts_with_existing_task_and_endpoint() {
		$endpointMock = $this->mockMake( EndpointMock::class );

		$this->backgroundTaskRepositoryMock->shouldReceive( 'getByTaskId' )->with( 1 )->andReturn( $this->backgroundTask );
		$this->updateBackgroundTaskMock->shouldReceive( 'startTask' )->with( $this->backgroundTask, $endpointMock )->andReturn( $this->backgroundTask );

		$res = $this->backgroundTaskService->startByTaskId( 1 );

		$this->assertTrue( $res === $this->backgroundTask );
	}

	/**
	 * @test
	 */
	public function it_throws_with_existing_task_and_wrong_endpoint() {
		$this->expectException( TaskIsNotRunnableException::class );

		$this->backgroundTask->setTaskType( WrongEndpointMock::class );
		$endpointMock = $this->mockMake( WrongEndpointMock::class );
		$this->backgroundTaskRepositoryMock->shouldReceive( 'getByTaskId' )->with( 1 )->andReturn( $this->backgroundTask );

		$this->backgroundTaskService->startByTaskId( 1 );
	}

	/**
	 * @test
	 */
	public function it_added_once_if_has_last_incompleted_by_type_and_has_no_records() {
		$endpointMock = $this->mockMake( EndpointMock::class );
		$payload      = wpml_collect( [] );

		$endpointMock->shouldReceive( 'getType' )->andReturn( EndpointMock::class );
		$endpointMock->shouldReceive( 'getTotalRecords' )->with( $payload )->andReturn( 0 );

		// Check in addOnce method.
		$this->backgroundTaskRepositoryMock->shouldReceive( 'getLastIncompletedByType' )->once()->with( EndpointMock::class )->andReturn( null );
		// Check in add method.
		$this->backgroundTaskRepositoryMock->shouldReceive( 'getLastIncompletedByType' )->once()->with( EndpointMock::class, [] )->andReturn( null );

		$this->persistBackgroundTaskMock->shouldNotReceive( 'run' );

		$res = $this->backgroundTaskService->addOnce( $endpointMock, $payload );

		$this->assertNull( $res );
	}

	/**
	 * @test
	 */
	public function it_added_once_and_executed_if_has_last_incompleted_by_type_and_has_records() {
		$endpointMock = $this->mockMake( EndpointMock::class );
		$payload      = wpml_collect( [] );

		$endpointMock->shouldReceive( 'getType' )->andReturn( EndpointMock::class );
		$endpointMock->shouldReceive( 'getTotalRecords' )->with( $payload )->andReturn( 2 );

		// Check in addOnce method.
		$this->backgroundTaskRepositoryMock->shouldReceive( 'getLastIncompletedByType' )->once()->with( EndpointMock::class )->andReturn( null );
		// Check in add method.
		$this->backgroundTaskRepositoryMock->shouldReceive( 'getLastIncompletedByType' )->once()->with( EndpointMock::class, [] )->andReturn( null );

		$this->persistBackgroundTaskMock->shouldReceive( 'run' )->with(
			$endpointMock->getType(), BackgroundTaskModel::TASK_STATUS_PENDING,
			2,
			$payload->toArray(),
			[]
		)->andReturn( $this->backgroundTask );

		$res = $this->backgroundTaskService->addOnce( $endpointMock, $payload );

		$this->assertTrue( $res === $this->backgroundTask );
	}

	/**
	 * @test
	 */
	public function it_not_added_once_if_has_no_background_task() {
		$endpointMock = $this->mockMake( EndpointMock::class );
		$payload      = wpml_collect( [] );

		$endpointMock->shouldReceive( 'getType' )->andReturn( EndpointMock::class );
		$endpointMock->shouldNotReceive( 'getTotalRecords' );
		$this->backgroundTaskRepositoryMock->shouldReceive( 'getLastIncompletedByType' )->with( EndpointMock::class )->andReturn( $this->backgroundTask );
		$this->persistBackgroundTaskMock->shouldNotReceive( 'run' );

		$res = $this->backgroundTaskService->addOnce( $endpointMock, $payload );

		$this->assertTrue( $res === $this->backgroundTask );
	}
}
