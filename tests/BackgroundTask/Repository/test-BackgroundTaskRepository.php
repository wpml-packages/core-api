<?php

namespace WPML\Core\BackgroundTask\Repository;

use WPML\API\MakeMock;
use WPML\Core\BackgroundTask\Command\DeleteBackgroundTask;
use WPML\LIB\WP\WPDBMock;
use WPML\Core\BackgroundTask\Model\BackgroundTask as BackgroundTaskModel;
use WPML\Core\BackgroundTask\Model\TaskEndpointInterface;
use WPML\FP\Str;
use function WPML\FP\pipe;

class TaskTypeHandlerMock implements TaskEndpointInterface {
	public function isDisplayed() { }

	public function isValidTask( $taskId ) { }

	public function getType() { }

	public function getMaxRetries() { }

	public function getLockTime() { }

	public function getTotalRecords( $data ) { }

	public function getDescription( $data ) { }

	public function run( $data ) { }
}

class Test_BackgroundTaskRepository extends \OTGS_TestCase {
	use MakeMock;
	use WPDBMock;

	private $backgroundTask;
	private $backgroundTaskRow;
	private $backgroundTaskRow2;

	private $taskTypeHandler;

	public function setUp() {
		parent::setUp();

		$this->setUpMakeMock();
		$this->setUpWPDBMock();

		$this->taskTypeHandler = $this->mockMake( TaskTypeHandlerMock::class );

		$this->backgroundTask = $this->make_backgroundtask();
		$this->backgroundTaskRow = [
			'task_id' => 1,
			'task_type' => TaskTypeHandlerMock::class,
			'task_status' => 0,
			'starting_date' => null,
			'total_count' => 0,
			'completed_count' => 0,
			'completed_ids' => null,
			'payload' => serialize( ['test' => 'test' ] ),
			'retry_count' => 0,
		];
		$this->backgroundTaskRow2 = [
			'task_id' => 2,
			'task_type' => TaskTypeHandlerMock::class,
			'task_status' => 0,
			'starting_date' => null,
			'total_count' => 0,
			'completed_count' => 0,
			'completed_ids' => null,
			'payload' => serialize( ['test2' => 'test2' ] ),
			'retry_count' => 0,
		];
	}

	private function make_backgroundtask( $data = array() ) {
		$task = new BackgroundTaskModel();
		$task->setTaskId( 1 );
		if ( array_key_exists( 'payload', $data ) ) {
			$task->setPayload( $data['payload'] );
		}
		$task->setTaskType( 'endpointClass' );
		$task->setPayload( ['test' => 'test'] );

		return $task;
	}

	private function get_query_matcher( $sqlFn ) {
		return pipe(
			$this->getTrimmedQuery(),
			Str::replace( "\t", '' ),
			Str::replace( "\n", ' ' ),
			Str::pregReplace( '/\s+/', ' ' ),
			$sqlFn
		);
	}

	/**
	 * @test
	 */
	public function it_gets_by_taskid() {
		global $wpdb;

		$this->addPrepareHandler(
			$this->get_query_matcher(
				Str::startsWith( "SELECT * FROM {$wpdb->prefix}" . BackgroundTaskModel::TABLE_NAME . " WHERE task_id = %s LIMIT 1" )
			),
			function( $query, $id ) {
				return ( 1 === $id ) ? $this->backgroundTaskRow : null;
			}
		);

		$backgroundTaskRepository = new BackgroundTaskRepository( $wpdb, $this->mockMake( DeleteBackgroundTask::class ) );
		$backgroundTask = $backgroundTaskRepository->getByTaskId( 1 );
		$this->assertEquals( 1, $backgroundTask->getTaskId() );
	}

	/**
	 * @test
	 */
	public function it_returns_last_incompleted_by_type() {
		global $wpdb;

		$this->addPrepareHandler(
			$this->get_query_matcher(
				Str::startsWith( "SELECT * FROM {$wpdb->prefix}" . BackgroundTaskModel::TABLE_NAME . " WHERE task_type = %s AND task_status != %s LIMIT 1" )
			),
			function( $query, $taskType, $taskStatus ) {
				return ( 'endpointClass' === $taskType && BackgroundTaskModel::TASK_STATUS_COMPLETED === $taskStatus ) ? $this->backgroundTaskRow : null;
			}
		);

		$backgroundTaskRepository = new BackgroundTaskRepository( $wpdb, $this->mockMake( DeleteBackgroundTask::class ) );
		$backgroundTask = $backgroundTaskRepository->getLastIncompletedByType( 'endpointClass' );
		$this->assertEquals( 1, $backgroundTask->getTaskId() );
	}

	/**
	 * @test
	 */
	public function it_returns_null_if_no_last_incompleted_by_type() {
		global $wpdb;

		$this->addPrepareHandler(
			$this->get_query_matcher(
				Str::startsWith( "SELECT * FROM {$wpdb->prefix}" . BackgroundTaskModel::TABLE_NAME . " WHERE task_type = %s AND task_status != %s LIMIT 1" )
			),
			function( $query, $taskType, $taskStatus ) {
				return null;
			}
		);

		$backgroundTaskRepository = new BackgroundTaskRepository( $wpdb, $this->mockMake( DeleteBackgroundTask::class ) );
		$backgroundTask = $backgroundTaskRepository->getLastIncompletedByType( 'endpointClass' );
		$this->assertNull( $backgroundTask );
	}

	/**
	 * @test
	 */
	public function it_returns_all_runnable_tasks() {
		global $wpdb;
		$statuses    = [ BackgroundTaskModel::TASK_STATUS_INPROGRESS, BackgroundTaskModel::TASK_STATUS_PENDING, BackgroundTaskModel::TASK_STATUS_PAUSED ];
		$statusesSql = implode( ", ", $statuses );

		$this->addPrepareHandler(
			$this->get_query_matcher(
				Str::startsWith( "SELECT * FROM {$wpdb->prefix}" . BackgroundTaskModel::TABLE_NAME . " WHERE task_status IN ({$statusesSql})" )
			),
			function( $query ) {
				return [
					$this->backgroundTaskRow,
					$this->backgroundTaskRow, // Duplicated.
					$this->backgroundTaskRow2
				];
			}
		);

		$this->taskTypeHandler->shouldReceive( 'isValidTask' )->times(2)->with(1)->andReturn( true );
		$this->taskTypeHandler->shouldReceive( 'isValidTask' )->once()->with(2)->andReturn( true );

		$deleteBackgroundTask = $this->mockMake( DeleteBackgroundTask::class );
		$deleteBackgroundTask->shouldReceive( 'run' );
		$backgroundTaskRepository = new BackgroundTaskRepository( $wpdb, $deleteBackgroundTask );
		$backgroundTasks = $backgroundTaskRepository->getAllRunnableTasks();

		// Only 2 of the 3 tasks, because the duplicated task (2 times $this->backgroundTaskRow) won't be returned.
		$this->assertCount( 2, $backgroundTasks );
		$this->assertEquals( 1, $backgroundTasks[0]->getTaskId() );
		$this->assertEquals( 2, $backgroundTasks[1]->getTaskId() );
	}

	/**
	 * @test
	 */
	public function it_returns_runnable_tasks_count() {
		global $wpdb;
		$statuses    = [ BackgroundTaskModel::TASK_STATUS_INPROGRESS, BackgroundTaskModel::TASK_STATUS_PENDING, BackgroundTaskModel::TASK_STATUS_PAUSED ];
		$statusesSql = implode( ", ", $statuses );

		$this->addPrepareHandler(
			$this->get_query_matcher(
				Str::startsWith( "SELECT COUNT(task_id) FROM {$wpdb->prefix}" . BackgroundTaskModel::TABLE_NAME . " WHERE task_status IN ({$statusesSql})" )
			),
			function( $query ) {
				return 5;
			}
		);

		$backgroundTaskRepository = new BackgroundTaskRepository( $wpdb, $this->mockMake( DeleteBackgroundTask::class ) );
		$count = $backgroundTaskRepository->getCountRunnableTasks();

		$this->assertEquals( 5, $count );
	}
}
