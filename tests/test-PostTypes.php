<?php

namespace WPML\API;

use WPML\LIB\WP\OptionMock;
use WPML\LIB\WP\PostTypeMock;
use WPML\Settings\PostType\Automatic;
use WPML\Setup\Option;

class Test_PostTypes extends \OTGS_TestCase {

	use PostTypesMock;
	use PostTypeMock;
	use OptionMock;

	public function setUp() {
		parent::setUp();

		$this->setUpPostTypesMock();
		$this->setUpOptionMock();
		$this->setUpPostTypeMock();
	}

	public function tearDown() {
		global $sitepress;
		$sitepress = null;

		parent::tearDown();
	}

	/**
	 * @test
	 */
	public function it_gets_translatable() {
		$this->assertEquals( [ 'page', 'post' ], PostTypes::getTranslatable() );

		$translatable = [ 'page', 'post', 'bock' ];
		$this->setTranslatablePostTypes( $translatable );
		$this->assertEquals( $translatable, PostTypes::getTranslatable() );
	}

	/**
	 * @test
	 */
	public function it_gets_translatable_with_info() {
		$expected = [
			'post' => $this->getPostTypes()['post'],
			'page' => $this->getPostTypes()['page'],
		];

		$this->assertEquals( $expected, PostTypes::getTranslatableWithInfo() );

		$this->setTranslatablePostTypes( [ 'page' ] );
		$expected = [ 'page' => $this->getPostTypes()['page'] ];
		$this->assertEquals( $expected, PostTypes::getTranslatableWithInfo() );
	}

	/**
	 * @test
	 */
	public function it_gets_display_as_translated() {
		$this->assertEquals( [], PostTypes::getDisplayAsTranslated() );

		$displayAsTranslated = [ 'page', 'post', 'bock' ];
		$this->setDisplayAsTranslatablePostTypes( $displayAsTranslated );
		$this->assertEquals( $displayAsTranslated, PostTypes::getDisplayAsTranslated() );
	}

	/**
	 * @test
	 */
	public function it_gets_only_translatable() {
		$this->assertEquals( [ 'page', 'post' ], PostTypes::getOnlyTranslatable() );

		$this->setDisplayAsTranslatablePostTypes( [ 'page' ] );
		$this->assertEquals( [ 'post' ], PostTypes::getOnlyTranslatable() );
	}

	/**
	 * @test
	 */
	public function post_types_without_a_setting_will_always_return_true() {
		$this->assertEquals( [ 'page', 'post' ], PostTypes::getAutomaticTranslatable() );

		$this->assertTrue( Automatic::isAutomatic( 'any_other_type' ) );
		$this->assertTrue( Automatic::isAutomatic( '' ), 'Empty string for post type should also return true' );
	}

	/**
	 * @test
	 */
	public function shouldTranslateAutomatically() {
		self::setPostTypeAutomatic( 'post', false );

		Option::setTMAllowed( true );
		Option::setTranslateEverything( true );
		$this->assertTrue( Automatic::shouldTranslate( 'page' ) );
		$this->assertFalse( Automatic::shouldTranslate( 'post' ) );

		Option::setTranslateEverything( false );
		$this->assertFalse( Automatic::shouldTranslate( 'page' ) );
		$this->assertFalse( Automatic::shouldTranslate( 'post' ) );

	}

	/**
	 * Attachment should be excluded
	 *
	 * @test
	 */
	public function it_gets_automatically_translatable() {
		$translatable = [ 'page', 'post', 'book', 'attachment' ];
		$this->setTranslatablePostTypes( $translatable );
		$this->assertEquals( [ 'page', 'post', 'book' ], PostTypes::getAutomaticTranslatable() );
	}

	/**
	 * @test
	 */
	public function it_gets_names() {
		$this->assertEquals( [ 'Posts', 'Pages' ], PostTypes::withNames( [ 'post', 'page' ] ) );
	}

	public static function setPostTypeAutomatic( $postType, $state ) {
		Automatic::saveFromConfig( [
			'wpml-config' => [
				'custom-types' => [
					'custom-type' => [
						[
							'value' => $postType,
							'attr'  => [ 'automatic' => $state ? '1' : '0' ]
						]
					]
				]
			]
		] );
	}
}
