<?php

namespace WPML\API;

use WPML\FP\Obj;

class Test_Settings extends \OTGS_TestCase {
	use SettingsMock;

	public function setUp() {
		parent::setUp();

		$this->setUpSettingsMock();
	}

	/**
	 * @test
	 */
	public function it_sets_and_gets() {
		$data = [ 'some' => 'data' ];
		$key  = 'something';

		$this->assertEquals( false, Settings::get( $key ) );
		$this->assertEquals( 'default', Settings::getOr( 'default', $key ) );

		Settings::set( $key, $data );

		$this->assertEquals( $data, Settings::get( $key ) );
		$this->assertEquals( $data, Settings::getOr( 'default', $key ) );
	}

	/**
	 * @test
	 */
	public function it_updates_subkey() {
		$data = [ 'some' => 'data' ];
		$key  = 'something';

		$subKey  = 'sub';
		$newData = 123;

		Settings::set( $key, $data );

		Settings::assoc( $key, $subKey, $newData );

		$this->assertEquals(
			Obj::assoc( $subKey, $newData, $data ),
			Settings::get( $key )
		);
	}

	/**
	 * @test
	 */
	public function it_gets_using_pathOr() {
		$data = [ 'level2' => 'value' ];
		$key  = 'level1';

		Settings::set( $key, $data );

		$this->assertEquals( 'value', Settings::pathOr( 'not found', [ $key, 'level2' ] ) );
		$this->assertEquals( 'not found', Settings::pathOr( 'not found', [ $key, 'xyz' ] ) );
		$this->assertEquals( 'not found', Settings::pathOr( 'not found', [ 'abc', 'xyz' ] ) );
	}

}


