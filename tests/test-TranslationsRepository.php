<?php

namespace WPML\Element\API;

use mocks\TranslationsRepositoryMock;
use WPML\LIB\WP\WPDBMock;

class Test_TranslationsRepository extends \OTGS_TestCase {
	use WPDBMock;
	use TranslationsRepositoryMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
		$this->setUpTranslationsRepository();
	}

	/**
	 * @test
	 */
	public function it_preloads_data_by_posts() {
		$data = [
			[
				'translation_id'       => '1',
				'element_id'           => '1',
				'element_type'         => 'post_post',
				'source_language_code' => null,
				'language_code'        => 'en',
				'trid'                 => '1',
				'rid'                  => null,
				'status'               => null,
				'job_id'               => null,
			],
			[
				'translation_id'       => '2',
				'element_id'           => '2',
				'element_type'         => 'post_post',
				'source_language_code' => 'en',
				'language_code'        => 'fr',
				'trid'                 => '1',
				'rid'                  => 1,
				'status'               => '10',
				'job_id'               => '1',
			],
			[
				'translation_id'       => '3',
				'element_id'           => '3',
				'element_type'         => 'post_post',
				'source_language_code' => 'en',
				'language_code'        => 'de',
				'trid'                 => '1',
				'rid'                  => '2',
				'status'               => '10',
				'job_id'               => '2',
			],
			[
				'translation_id'       => '4',
				'element_id'           => '4',
				'element_type'         => 'post_post',
				'source_language_code' => null,
				'language_code'        => 'en',
				'trid'                 => '2',
				'rid'                  => null,
				'status'               => null,
				'job_id'               => null,
			],
			[
				'translation_id'       => '5',
				'element_id'           => '5',
				'element_type'         => 'post_post',
				'source_language_code' => 'en',
				'language_code'        => 'de',
				'trid'                 => '2',
				'rid'                  => '3',
				'status'               => '2',
				'job_id'               => '3',
			],
			[
				'translation_id'       => '6',
				'element_id'           => '6',
				'element_type'         => 'post_post',
				'source_language_code' => null,
				'language_code'        => 'en',
				'trid'                 => '3',
				'rid'                  => null,
				'status'               => null,
				'job_id'               => null,
			],
		];

		$this->addPostTranslationRecordsToPreload( 1, 'post_type', [ $data[0], $data[1], $data[2] ] );
		$this->addPostTranslationRecordsToPreload( 4, 'post_type', [ $data[3], $data[4] ] );
		$this->addPostTranslationRecordsToPreload( 6, 'post_type', [ $data[5] ] );

		TranslationsRepository::preloadForPosts( [
			[ 'ID' => 1, 'post_type' => 'post' ],
			[ 'ID' => 4, 'post_type' => 'post' ],
			[ 'ID' => 6, 'post_type' => 'post' ],
		] );

		$this->assertEquals( $data[1], TranslationsRepository::getByTridAndLanguage( 1, 'fr' ) );
		$this->assertEquals( $data[1], TranslationsRepository::getByTranslationId( 2 ) );

		$this->assertEquals( $data[4], TranslationsRepository::getByTridAndLanguage( 2, 'de' ) );
		$this->assertEquals( $data[4], TranslationsRepository::getByTranslationId( 5 ) );

		$this->assertEquals( $data[5], TranslationsRepository::getByTridAndLanguage( 3, 'en' ) );
		$this->assertEquals( $data[5], TranslationsRepository::getByTranslationId( 6 ) );

		$this->assertNull( TranslationsRepository::getByTridAndLanguage( 3, 'fr' ) );
		$this->assertNull( TranslationsRepository::getByTranslationId( 8 ) );
	}

}