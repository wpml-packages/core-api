<?php

namespace WPML\Element\API;

use tad\FunctionMocker\FunctionMocker;
use WPML\FP\Fns;
use WPML\FP\Logic;
use WPML\FP\Lst;
use WPML\FP\Obj;
use WPML\FP\Relation;
use WPML\LIB\WP\OptionMock;
use WPML\LIB\WP\User;
use WPML\LIB\WP\WPDBMock;
use WPML\API\SettingsMock;

class Test_Languages extends \OTGS_TestCase {
	use WPDBMock;
	use OptionMock;
	use SettingsMock;
	use LanguagesMock;

	/** @var string|null */
	public $userLanguage;

	public function setUp() {
		parent::setUp();

		$this->setUpSettingsMock();
		$this->setUpWPDBMock();
		$this->setupLanguagesMock();
		$this->setUpOptionMock();
	}

	public function tearDown() {

		$this->tearDownLanguagesMock();
		parent::tearDown();
	}

	/**
	 * @test
	 */
	public function itReturnsActiveLanguages() {
		$activeLangs = [ 'en', 'de', 'fr', 'pl' ];
		$this->setActiveLanguages( $activeLangs );

		$this->assertCount( count( $activeLangs ), Languages::getActive() );
		$this->assertEquals( $activeLangs, array_keys( Languages::getActive() ) );

		foreach ( Languages::getActive() as $language ) {
			$this->assertTrue( Obj::prop( 'built_in', $language ) );
		}
	}

	/**
	 * @test
	 */
	public function it_gets_default() {
		$this->setDefaultLanguage( 'en' );

		$this->assertEquals( Obj::assoc( 'built_in', true, $this->availableLanguages['en'] ), Languages::getDefault() );

		$this->setDefaultLanguage( 'fr' );
		$this->assertEquals( Obj::assoc( 'built_in', true, $this->availableLanguages['fr'] ), Languages::getDefault() );
	}

	/**
	 * @test
	 */
	public function it_gets_default_code() {
		$this->setDefaultLanguage( 'en' );

		$this->assertEquals(
			'en',
			Languages::getDefaultCode()
		);
	}

	/**
	 * @test
	 */
	public function it_gets_secondaries() {
		$this->setActiveLanguages( [ 'en', 'de', 'fr', 'pl' ] );
		$this->setDefaultLanguage( 'en' );

		$activeLangs = Languages::getActive();

		$secondaries = Languages::getSecondaries();
		$expected    = [
			'de' => $activeLangs['de'],
			'fr' => $activeLangs['fr'],
			'pl' => $activeLangs['pl'],
		];

		$this->assertEquals( $expected, $secondaries );
	}

	/**
	 * @test
	 */
	public function it_gets_secondary_codes() {
		$activeLangs = [ 'en', 'de', 'fr', 'pl' ];
		$this->setActiveLanguages( $activeLangs );
		$this->setDefaultLanguage( 'en' );

		$secondaries = Languages::getSecondaryCodes();
		$this->assertEquals(
			['de', 'fr', 'pl'],
			$secondaries
		);
	}

	/**
	 * @test
	 */
	public function itGetsFlagURL() {
		foreach ( $this->activeLanguages as $lang ) {
			$this->assertEquals( $this->flags[ $lang ]['flag'], Languages::getFlagUrl( $lang ) );
		}
	}

	/**
	 * @test
	 */
	public function itAppendsFlagInfo() {
		$langs = Languages::withFlags( Languages::getActive() );
		foreach ( $langs as $code => $lang ) {
			$this->assertEquals( $this->flags[ $code ]['flag'], $lang['flag_url'] );
			$this->assertEquals( $this->flags[ $code ]['from_template'], $lang['flag_from_template'] );
		}
	}

	/**
	 * @test
	 */
	public function itGetsRrl() {
		$this->assertFalse( Languages::isRtl( 'en' ) );
		$this->assertFalse( Languages::isRtl( 'de' ) );
		$this->assertTrue( Languages::isRtl( 'he' ) );
	}

	/**
	 * @test
	 */
	public function itAddsRtl() {
		$activeLangs = [ 'en', 'de', 'fr', 'he' ];
		$this->setActiveLanguages( $activeLangs );

		$langs = Languages::withRtl( Languages::getActive() );
		foreach ( $langs as $code => $lang ) {
			$this->assertEquals( $this->rtl[ $code ], $lang['rtl'] );
		}
	}


	/**
	 * @test
	 */
	public function itGetsAllLanguages() {
		$this->assertCount( count( $this->availableLanguages ), Languages::getAll() );
	}

	/**
	 * @test
	 */
	public function itGetsAllLanguagesInLang() {
		$languageNames = Fns::map( Obj::prop( 'display_name' ), Languages::getAll( 'pl' ) );
		$this->assertEquals(
			[
				'en' => 'Angielski',
				'de' => 'Niemiecki',
				'fr' => 'Francuski',
				'es' => 'Hiszpański',
				'ru' => 'Rosyjski',
				'pl' => 'Polski',
				'he' => 'Hebrajski',
				'zu' => '',
				'pt-pt' => 'Portugalski, Portugalia',
				'pt-br' => 'Portugalski, Brazylia',
			],
			$languageNames
		);
	}

	/**
	 * @test
	 */
	public function itGetsCurrentLanguageCodee() {
		$this->setCurrentLanguage( 'de' );
		$this->assertEquals( 'de', Languages::getCurrentCode() );

		$this->setCurrentLanguage( 'fr' );
		$this->assertEquals( 'fr', Languages::getCurrentCode() );
	}


	/**
	 * @test
	 */
	public function itGetsNullWhenDefaultLanguageIsNotDefined() {
		$this->defaultLanguage = null;

		$this->assertNull( Languages::getDefault() );
	}

	/**
	 * @test
	 */
	public function itGetsLanguageDetails() {
		$this->assertEquals( Obj::assoc( 'built_in', true, $this->availableLanguages['pl'] ), Languages::getLanguageDetails( 'pl' ) );
	}

	/**
	 * @test
	 */
	public function itReturnsFalseWhenLangaugeIsNotDefined() {
		$this->assertFalse( Languages::getLanguageDetails( 'all' ) );
	}

	/**
	 * @test
	 * @group pierre
	 */
	public function itGetsCodeFromLocale() {
		$this->assertEquals( 'en', Languages::localeToCode( 'en_US' ) );
		$this->assertEquals( 'en', Languages::localeToCode( 'en_GB' ) );
		$this->assertEquals( 'fr', Languages::localeToCode( 'fr_FR' ) );
		$this->assertEquals( 'fr', Languages::localeToCode( 'fr_CA' ) );
		$this->assertEquals( 'pt-br', Languages::localeToCode( 'pt_BR' ) );
		$this->assertEquals( 'pt-pt', Languages::localeToCode( 'pt_PT' ) );

		$this->assertEquals( 'zu', Languages::localeToCode( 'zu' ) ); // guessed code
		$this->assertEquals( 'zu', Languages::localeToCode( 'zu_SOMETHING' ) ); // guessed code

		$this->assertEquals( false, Languages::localeToCode( 'wrong_WRONG' ) );
		$this->assertEquals( false, Languages::localeToCode( '' ) );
	}

	/**
	* @test
	*/
	public function itSetsLanguageTranslation() {
		$langCode    = 'fr';
		$displayLang = 'de';
		$name        = 'Französisch';

		Languages::setLanguageTranslation( $langCode, $displayLang, $name );

		$this->assertCount( 1, $this->languageTranslations );
		$this->assertEquals( [
			'id'                    => 1,
			'language_code'         => $langCode,
			'display_language_code' => $displayLang,
			'name'                  => $name,
		], Lst::last( $this->languageTranslations ) );
	}

	/**
	 * @test
	 */
	public function it_adds_language() {
		$availableLanguages = $this->availableLanguages;

		$langId = Languages::add(
			'te',
			'TEST',
			'te_EU',
			0,
			0,
			0,
			'te_tag',
			'de'
		);

		$this->assertEquals( 11, $langId );

		$allLangs = Languages::getAll();

		$this->assertArrayHasKey( 'te', $allLangs );
		$addedLang = $allLangs['te'];
		$this->assertEquals( 'te', $addedLang['code'] );
		$this->assertEquals( 'TEST', $addedLang['english_name'] );
		$this->assertEquals( 'TEST', $addedLang['native_name'] );
		$this->assertEquals( 'te_EU', $addedLang['default_locale'] );
		$this->assertEquals( 'te_tag', $addedLang['tag'] );
		$this->assertEquals( 0, $addedLang['encode_url'] );
		$this->assertEquals( 'de', $addedLang['country'] );

		$findTranslation = function ( $code, $displayLangCode ) {
			return Lst::find( Logic::both(
				Relation::propEq( 'language_code', $code ),
				Relation::propEq( 'display_language_code', $displayLangCode )
			), $this->languageTranslations );
		};

		foreach ( $availableLanguages as $code => $data ) {
			$this->assertEquals( 'TEST', Obj::prop( 'name', $findTranslation( 'te', $code ) ) );
			$this->assertEquals( $data['english_name'], Obj::prop( 'name', $findTranslation( $code, 'te' ) ) );
		}

		$this->assertEquals( 'TEST', Obj::prop( 'name', $findTranslation( 'te', 'te' ) ) );
	}

	/**
	 * @test
	 */
	public function it_gets_user_language_code() {
		$fallbackCode = 'en';
		$userLanguageCode = 'de';

		FunctionMocker::replace( User::class . '::getCurrent', function () {
			return  $this->userLanguage ? (object) [ 'locale' => $this->userLanguage ] : null;
		} );

		$this->userLanguage = 'de_DE';
		$this->assertSame( $userLanguageCode, Languages::getUserLanguageCode()->getOrElse( $fallbackCode ) );

		$this->userLanguage = null;
		$this->assertSame( $fallbackCode, Languages::getUserLanguageCode()->getOrElse( $fallbackCode ) );
	}

	/**
	* @test
	*/
	public function it_gets_wp_locale() {
		$langDetails = [
			'code'           => 'fr',
			'tag'            => 'fr-fr',
			'default_locale' => 'fr_FR',
		];

		// get by default locale
		$this->wpLocalesMap = [];
		$this->addWPLocaleMapping( 'fr_FR', 'fr_FR_WP' );
		$this->assertEquals( 'fr_FR_WP', Languages::getWPLocale( $langDetails ) );

		// get by tag
		$this->wpLocalesMap = [];
		$this->addWPLocaleMapping( 'fr-fr', 'fr_FR_WP' );
		$this->assertEquals( 'fr_FR_WP', Languages::getWPLocale( $langDetails ) );

		// get by code
		$this->wpLocalesMap = [];
		$this->addWPLocaleMapping( 'fr', 'fr_FR_WP' );
		$this->assertEquals( 'fr_FR_WP', Languages::getWPLocale( $langDetails ) );

		// get default_locale as fallback
		$this->wpLocalesMap = [];
		$defaultLangDetails = [
			'code' => 'fr_default',
			'tag' => 'fr-fr_default',
			'default_locale' => 'fr_FR_default',
		];
		$this->assertEquals( 'fr_FR_default', Languages::getWPLocale( $defaultLangDetails ) );
	}

	/**
	 * @test
	 */
	public function it_caches_wp_download_locale() {
		$this->wpLocalesMap = [];
		$this->addWPLocaleMapping( 'es_es', 'es_ES_WP' );
		$this->assertEquals( 'es_ES_WP', Languages::downloadWPLocale( 'es_es' ) );

		// Check that after second call, we don't call wp_download_language_pack but get the cached value instead.
		$this->wpLocalesMap = [];
		$this->addWPLocaleMapping( 'es_es',  'THIS SHOULD NOT BE RETURNED' );
		$this->assertEquals( 'es_ES_WP', Languages::downloadWPLocale( 'es_es' ) );

	}

	/**
	 * @test
	 */
	public function it_retries_wp_download_locale_when_false() {
		$this->wpLocalesMap = [];
		$this->addWPLocaleMapping( 'es_es',  false );
		$this->assertEquals( false, Languages::downloadWPLocale( 'es_es' ) );

		$this->wpLocalesMap = [];
		$this->addWPLocaleMapping( 'es_es',  'es_ES_WP' );
		$this->assertEquals( 'es_ES_WP', Languages::downloadWPLocale( 'es_es' ) );

		// Check that after second call, we don't call wp_download_language_pack but get the cached value instead.
		$this->wpLocalesMap = [];
		$this->addWPLocaleMapping( 'es_es',  'THIS SHOULD NOT BE RETURNED' );
		$this->assertEquals( 'es_ES_WP', Languages::downloadWPLocale( 'es_es' ) );

	}

	/**
	 * @test
	 */
	function it_gets_language_code_by_display_name() {
		$this->setCurrentLanguage( 'en' );
		$this->assertEquals( 'fr', Languages::getCodeByName( 'French' ) );
		$this->assertEquals( 'de', Languages::getCodeByName( 'German' ) );
		$this->assertEquals( 'en', Languages::getCodeByName( 'English' ) );

		$this->setCurrentLanguage( 'de' );
		$this->assertEquals( 'fr', Languages::getCodeByName( 'Französisch' ) );
		$this->assertEquals( 'de', Languages::getCodeByName( 'Deutsch' ) );
		$this->assertEquals( 'en', Languages::getCodeByName( 'Englisch' ) );

		$this->setCurrentLanguage( 'fr' );
		$this->assertEquals( 'fr', Languages::getCodeByName( 'Français' ) );
		$this->assertEquals( 'de', Languages::getCodeByName( 'Allemand' ) );
		$this->assertEquals( 'en', Languages::getCodeByName( 'Anglais' ) );
	}

	/**
	 * @test
	 */
	public function test_whileInLanguage() {
		$lang         = 'de';
		$originalLang = $this->currentLanguage;
		$arg          = 123;

		FunctionMocker::replace( 'my_function', function ( $arg1 ) use ( $lang ) {
			$this->assertEquals( $lang, $this->currentLanguage );

			return 'Function called with ' . $arg1;
		} );

		$result = Languages::whileInLanguage( $lang )
		                 ->invoke( 'my_function' )
		                 ->runWith( $arg );

		$this->assertEquals( 'Function called with ' . $arg, $result );

		$this->assertDidSwitchLanguages( $originalLang, $lang, $originalLang );
	}
}
