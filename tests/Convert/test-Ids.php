<?php

namespace WPML\Convert;

use WPML\FP\Obj;

/**
 * @group compdev-12
 */
class TestIds extends \OTGS_TestCase {

	const IDS_MAP = [
		123 => 124,
		235 => 236,
	];

	const ELEMENT_TYPE = 'something';
	const TARGET_LANG  = 'fr';

	public function setUp() {
		global $sitepress;

		parent::setUp();

		$sitepress = $this->getMockBuilder( \Sitepress::class )
		                  ->setMethods( [ 'get_object_id' ] )
		                  ->disableOriginalConstructor()->getMock();

		$sitepress->method( 'get_object_id' )
		          ->willReturnCallback( function( $id, $elementType, $fallbackToOriginal, $targetLang ) {
			          $this->assertEquals( self::ELEMENT_TYPE, $elementType );
			          $this->assertEquals( self::TARGET_LANG, $targetLang );

			          return Obj::propOr( $fallbackToOriginal ? $id : null, $id, self::IDS_MAP );
		          } );
	}

	public function tearDown() {
		parent::tearDown();
		unset( $GLOBALS['sitepress'] );
	}

	/**
	 * @test
	 * @dataProvider dpConvertWithoutFallback
	 *
	 * @param int|string|mixed $original
	 * @param int|string|mixed $expected
	 *
	 * @return void
	 */
	public function convertWithoutFallback( $original, $expected ) {
		$result = Ids::convert( $original, self::ELEMENT_TYPE, false, self::TARGET_LANG );

		if ( is_array( $original ) || is_object( $original ) ) {
			$this->assertEquals( $expected, $result );
		} else {
			$this->assertSame( $expected, $result );
		}
	}

	public function dpConvertWithoutFallback() {
		return [
			'integer existing'       => [ 123, 124 ],
			'integer non-existing'   => [ 999, null ],
			'numerical existing'     => [ '123', '124' ],
			'numerical non-existing' => [ '999', null ],
			'serialized array'       => [ serialize( [ 123, '235', 999 ] ), serialize( [ 124, '236' ] ) ],
			'JSON encoded array'     => [ json_encode( [ 123, '235', 999 ] ), json_encode( [ 124, '236' ] ) ],
			'string list comma'      => [ '123,999,235,999', '124,236' ],
			'string list semi-colon' => [ '123;999;235;999', '124;236' ],
			'string list pipe'       => [ '123|999|235|999', '124|236' ],
			'string list plus'       => [ '123+999+235+999', '124+236' ],
			'string list slash'      => [ '123/999/235/999', '124/236' ],
			'string list complex'    => [ '123<|>999<|>235<|>999', '124<|>236' ],
			'array'                  => [ [ 123, '235', 999 ], [ 124, '236' ] ],

			// Unsupported types or formats - We do not convert and return the original input.
			'true'                   => [ true, true ],
			'false'                  => [ false, false ],
			'null'                   => [ null, null ],
			'float'                  => [ 123.235, 123.235 ],
			'non-numerical string'   => [ 'something', 'something' ],
			'hash-like'              => [ '7126febf0b5f', '7126febf0b5f' ],
			'object'                 => [ (object) [ 123 ], (object) [ 123 ] ],
			'serialized object'      => [ serialize( (object) [ 123 ] ), serialize( (object) [ 123 ] ) ],
			'JSON encoded object'    => [ json_encode( (object) [ 123 ] ), json_encode( (object) [ 123 ] ) ],
			'array of non-numerical' => [ [ 'something' ], [ 'something' ] ],
			'array of non-scalar'    => [ [ [ 123 ] ], [ [ 123 ] ] ],
		];
	}

	/**
	 * @test
	 * @dataProvider dpConvertWithFallback
	 *
	 * @param int|string|mixed $original
	 * @param int|string|mixed $expected
	 *
	 * @return void
	 */
	public function convertWithFallback( $original, $expected ) {
		$result = Ids::convert( $original, self::ELEMENT_TYPE, true, self::TARGET_LANG );

		if ( is_array( $original ) || is_object( $original ) ) {
			$this->assertEquals( $expected, $result );
		} else {
			$this->assertSame( $expected, $result );
		}
	}

	public function dpConvertWithFallback() {
		return [
			'integer existing'       => [ 123, 124 ],
			'integer non-existing'   => [ 999, 999 ],
			'numerical existing'     => [ '123', '124' ],
			'numerical non-existing' => [ '999', '999' ],
			'serialized array'       => [ serialize( [ 123, '235', 999 ] ), serialize( [ 124, '236', 999 ] ) ],
			'JSON encoded array'     => [ json_encode( [ 123, '235', 999 ] ), json_encode( [ 124, '236', 999 ] ) ],
			'string list comma'      => [ '123,999,235,999', '124,999,236,999' ],
			'string list semi-colon' => [ '123;999;235;999', '124;999;236;999' ],
			'string list pipe'       => [ '123|999|235|999', '124|999|236|999' ],
			'string list plus'       => [ '123+999+235+999', '124+999+236+999' ],
			'string list slash'      => [ '123/999/235/999', '124/999/236/999' ],
			'string list complex'    => [ '123<|>999<|>235<|>999', '124<|>999<|>236<|>999' ],
			'array'                  => [ [ 123, '235', 999 ], [ 124, '236', 999 ] ],

			// Unsupported types or formats - We do not convert and return the original input.
			'true'                   => [ true, true ],
			'false'                  => [ false, false ],
			'null'                   => [ null, null ],
			'float'                  => [ 123.235, 123.235 ],
			'non-numerical string'   => [ 'something', 'something' ],
			'hash-like'              => [ '7126febf0b5f', '7126febf0b5f' ],
			'object'                 => [ (object) [ 123 ], (object) [ 123 ] ],
			'serialized object'      => [ serialize( (object) [ 123 ] ), serialize( (object) [ 123 ] ) ],
			'JSON encoded object'    => [ json_encode( (object) [ 123 ] ), json_encode( (object) [ 123 ] ) ],
			'array of non-numerical' => [ [ 'something' ], [ 'something' ] ],
			'array of non-scalar'    => [ [ [ 123 ] ], [ [ 123 ] ] ],
		];
	}

	/**
	 * @test
	 * @dataProvider dpConvertWithFallback
	 *
	 * @param int|string|mixed $original
	 * @param int|string|mixed $expected
	 *
	 * @return void
	 */
	public function convertWithFallbackWithAnyPost( $original, $expected ) {
		\WP_Mock::userFunction( 'get_post_type', [
			'times'  => '0-1', // Make sure we detect the type on the first ID only (0 is for unsupported formats).
			'args'   => [ 123 ],
			'return' => self::ELEMENT_TYPE,
		] );

		$result = Ids::convert( $original, Ids::ANY_POST, true, self::TARGET_LANG );

		if ( is_array( $original ) || is_object( $original ) ) {
			$this->assertEquals( $expected, $result );
		} else {
			$this->assertSame( $expected, $result );
		}
	}

	/**
	 * @test
	 * @dataProvider dpConvertWithFallback
	 *
	 * @param int|string|mixed $original
	 * @param int|string|mixed $expected
	 *
	 * @return void
	 */
	public function convertWithFallbackWithAnyTerm( $original, $expected ) {
		$term           = \Mockery::mock( \WP_Term::class );
		$term->taxonomy = self::ELEMENT_TYPE;

		\WP_Mock::userFunction( 'get_term', [
			'times'  => '0-1', // Make sure we detect the type on the first ID only (0 is for unsupported formats).
			'args'   => [ 123 ],
			'return' => $term,
		] );

		$result = Ids::convert( $original, Ids::ANY_TERM, true, self::TARGET_LANG );

		if ( is_array( $original ) || is_object( $original ) ) {
			$this->assertEquals( $expected, $result );
		} else {
			$this->assertSame( $expected, $result );
		}
	}

	/**
	 * @test
	 * @group wpmldev-1193
	 *
	 * @return void
	 */
	public function itShouldConvertEmptyString() {
		$this->assertSame( '', Ids::convert( '', self::ELEMENT_TYPE, true, self::TARGET_LANG ) );
	}
}
