<?php

namespace WPML\API;

class Test_Sanitize extends \OTGS_TestCase {

	/**
	 * @test
	 * @dataProvider stringProvider
	 */
	public function itSanitizesString($rawString, $expectedResult) {
		$this->assertEquals( $expectedResult, Sanitize::string( $rawString ) );
	}

	/**
	 * @test
	 */
	public function itSanitizesStringWithFlag() {
		$this->assertEquals( '"\'', Sanitize::string( '"\'', ENT_NOQUOTES ) );
	}

	/**
	 * @test
	 */
	public function itSanitizesStringProperty() {
		$property         = 'property';
		$rawValue         = "\0aäÆØÅ!'api<p></p>";
		$arr[ $property ] = $rawValue;

		$this->assertEquals( 'aäÆØÅ!&#039;api', Sanitize::stringProp( $property, $arr ) );
	}

	/**
	 * @test
	 */
	public function itReturnsNullIfFailedSanitizationProperty() {
		$property         = 'property';
		$arr = [];

		$this->assertNull( Sanitize::stringProp( $property, $arr ) );
	}

	public function stringProvider() {
		return [
			[ "\0aäÆØÅ!'api", 'aäÆØÅ!&#039;api' ],
			[ [], false ],
			[ null, '' ],
			[ '', '' ],
			[ '"\'', '&quot;&#039;' ],
			[ '&', '&' ],
			[ '&amp;', '&amp;' ],
			[ '123', '123' ],
			[ 123, '123' ],
			[ 123.123, '123.123' ],
			[ "\0aäÆØÅ!'api<p></p>", 'aäÆØÅ!&#039;api' ],
		];
	}
}