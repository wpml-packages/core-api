<?php

namespace WPML\API;

use tad\FunctionMocker\FunctionMocker;
use WPML\Timer;

class Test_Timer extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function test() {

		$currentTime = 123456;
		$timeOut = 20;

		FunctionMocker::replace( 'time', $currentTime );

		$timer = new Timer();
		$timer->start( $timeOut );

		$this->assertFalse( $timer->hasTimedOut() );
		$this->assertTrue( $timer->hasNotTimedOut() );

		$currentTime += $timeOut + 1;
		FunctionMocker::replace( 'time', $currentTime );

		$this->assertTrue( $timer->hasTimedOut() );
		$this->assertFalse( $timer->hasNotTimedOut() );
	}

}


