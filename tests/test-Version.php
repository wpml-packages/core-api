<?php

namespace WPML\API;

class Test_Version extends \OTGS_TestCase {
	use WPMLInstallationMock;

	public function setUp() {
		parent::setUp();

		$this->setupInstallationMock();
	}

	/**
	 * @test
	 */
	public function itGetsTheFirstInstallationVersion() {
		$this->setFirstInstallationWPMLVersion('4.2.1');

		$this->assertEquals( '4.2.1', Version::firstInstallation() );
	}

	/**
	 * @test
	 */
	public function test_isHigherThanInstallation() {
		$this->setFirstInstallationWPMLVersion('4.2.1');

		$this->assertFalse( Version::isHigherThanInstallation( '4.2.0' ) );
		$this->assertFalse( Version::isHigherThanInstallation( '3.4.1' ) );
		$this->assertFalse( Version::isHigherThanInstallation( '4.2.1' ) );

		$this->assertTrue( Version::isHigherThanInstallation( '4.2.2' ) );
		$this->assertTrue( Version::isHigherThanInstallation( '5.5.0' ) );
	}
}


