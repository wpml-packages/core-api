<?php

namespace WPML\Element\API;


use WPML\FP\Fns;
use WPML\FP\Obj;
use WPML\LIB\WP\PostMock;
use WPML\LIB\WP\WPDBMock;

class Test_PostTranslations extends \OTGS_TestCase {
	use WPDBMock;
	use TranslationsMock;
	use PostMock;
	use LanguagesMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
		$this->setUpPostMock();
		$this->setupLanguagesMock();
		$this->setupElementTranslations();
	}

	public function tearDown() {
		$this->tearDownLanguagesMock();
		parent::tearDown();
	}

	/**
	 * @test
	 */
	public function it_sets_original_translation() {
		$postId   = 123;
		$postType = 'page';
		$lang     = 'en';

		$this->mockPostType( $postId, $postType );
		PostTranslations::setAsSource( $postId, $lang );

		$translations = PostTranslations::get( $postId );

		$this->assertCount( 1, $translations );

		$original = Obj::prop( Fns::__, $translations[$lang] );

		$this->assertTrue( $original( 'original' ) );
		$this->assertEquals( $postId, $original( 'element_id' ) );
		$this->assertNull( $original( 'source_language_code' ) );
		$this->assertEquals( $lang, $original( 'language_code' ) );

		$this->assertTrue( Translations::isOriginal( $postId, $translations ) );
	}

	/**
	 * @test
	 */
	public function it_sets_translation() {
		$postId          = 123;
		$translationId   = 456;
		$postType        = 'page';
		$lang            = 'en';
		$translationLang = 'de';

		$this->mockPostType( $postId, $postType );
		PostTranslations::setAsSource( $postId, $lang );
		PostTranslations::setAsTranslationOf( $postId, $translationId, $translationLang );

		$translations = PostTranslations::get( $postId );

		$this->assertCount( 2, $translations );

		$original = Obj::prop( Fns::__, $translations[$lang] );

		$this->assertTrue( $original( 'original' ) );
		$this->assertEquals( $postId, $original( 'element_id' ) );
		$this->assertNull( $original( 'source_language_code' ) );

		$translation = Obj::prop( Fns::__, $translations[$translationLang] );

		$this->assertFalse( $translation( 'original' ) );
		$this->assertEquals( $translationId, $translation( 'element_id' ) );
		$this->assertEquals( $lang, $translation( 'source_language_code' ) );

		$this->assertEquals( $original( 'trid' ), $translation( 'trid' ) );

		$this->assertTrue( Translations::isOriginal( $postId, $translations ) );
	}

	/**
	 * @test
	 */
	public function it_gets_translations_only_if_original() {
		$postId          = 123;
		$translationId   = 456;
		$postType        = 'page';
		$lang            = 'en';
		$translationLang = 'de';

		$this->mockPostType( $postId, $postType );
		$this->mockPostType( $translationId, $postType );
		PostTranslations::setAsSource( $postId, $lang );
		PostTranslations::setAsTranslationOf( $postId, $translationId, $translationLang );

		$this->assertCount( 2, PostTranslations::get( $postId ) );
		$this->assertCount( 2, PostTranslations::getIfOriginal( $postId ) );

		$this->assertCount( 2, PostTranslations::get( $translationId ) );
		$this->assertCount( 0, PostTranslations::getIfOriginal( $translationId ) );

	}

	/**
	 * @test
	 */
	public function it_gets_translation_in_language() {
		$postId          = 123;
		$translationId   = 456;
		$postType        = 'page';
		$lang            = 'en';
		$translationLang = 'de';

		$this->mockPostType( $postId, $postType );
		$this->mockPostType( $translationId, $postType );
		PostTranslations::setAsSource( $postId, $lang );
		PostTranslations::setAsTranslationOf( $postId, $translationId, $translationLang );

		$translation = PostTranslations::getInLanguage( $postId, $translationLang );

		$this->assertEquals( $translationId, $translation->element_id );

		$this->assertNull( PostTranslations::getInLanguage( $postId, 'missing-language' ) );
	}

	/**
	 * @test
	 */
	public function it_gets_translation_in_current_language() {
		$postId          = 123;
		$translationId   = 456;
		$postType        = 'page';
		$lang            = 'en';
		$currentLanguage = 'de';

		$this->setCurrentLanguage( $currentLanguage );

		$this->mockPostType( $postId, $postType );
		$this->mockPostType( $translationId, $postType );
		PostTranslations::setAsSource( $postId, $lang );
		PostTranslations::setAsTranslationOf( $postId, $translationId, $currentLanguage );

		$translation = PostTranslations::getInCurrentLanguage( $postId );

		$this->assertEquals( $translationId, $translation->element_id );
		$this->assertEquals( $currentLanguage, $translation->language_code );
	}

	/**
	 * @test
	 */
	public function itGetsOriginal() {
		$postId          = 123;
		$translationId   = 456;
		$postType        = 'page';
		$lang            = 'en';
		$translationLang = 'de';

		$this->mockPostType( $postId, $postType );
		$this->mockPostType( $translationId, $postType );
		PostTranslations::setAsSource( $postId, $lang );
		PostTranslations::setAsTranslationOf( $postId, $translationId, $translationLang );

		$this->assertSame( $postId, PostTranslations::getOriginalId( $postId ) );
		$this->assertSame( $postId, PostTranslations::getOriginalId( $translationId ) );

		foreach ( [
				PostTranslations::getOriginal( $postId ),
				PostTranslations::getOriginal( $translationId )
			] as $element
		) {
			$this->assertTrue( $element->original );
			$this->assertEquals( $postId, $element->element_id );
			$this->assertNull( $element->source_language_code );
			$this->assertEquals( $lang, $element->language_code );
		}

		$this->assertNull( PostTranslations::getOriginal( 'non_existing_id' ) );
		$this->assertSame( 0, PostTranslations::getOriginalId( 'non_existing_id' ) );
	}
}
