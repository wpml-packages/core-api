<?php

namespace WPML\Element\API;

use WPML\FP\Fns;
use WPML\FP\Obj;
use WPML\LIB\WP\PostMock;
use WPML\LIB\WP\WPDBMock;

class Test_IfOriginalPost extends \OTGS_TestCase {
	use WPDBMock;
	use TranslationsMock;
	use PostMock;

	const POST_ID         = 123;
	const TRANSLATED_ID   = 456;
	const LANG            = 'en';
	const TRANSLATED_LAND = 'de';

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
		$this->setUpPostMock();
		$this->setupElementTranslations();

		$this->mockPostType( self::POST_ID, 'page' );
		PostTranslations::setAsSource( self::POST_ID, self::LANG );
	}

	/**
	 * @test
	 */
	public function it_returns_empty_if_no_translations() {
		$this->assertEmpty( IfOriginalPost::getTranslations( self::POST_ID ) );
		$this->assertEmpty( IfOriginalPost::getTranslationIds( self::POST_ID ) );
	}

	/**
	 * @test
	 */
	public function it_returns_empty_if_not_original() {
		PostTranslations::setAsTranslationOf( self::POST_ID, self::TRANSLATED_ID, self::TRANSLATED_LAND );

		$this->assertEmpty( IfOriginalPost::getTranslations( self::TRANSLATED_ID ) );
		$this->assertEmpty( IfOriginalPost::getTranslationIds( self::TRANSLATED_ID ) );
	}

	/**
	 * @test
	 */
	public function it_returns_translations() {
		PostTranslations::setAsTranslationOf( self::POST_ID, self::TRANSLATED_ID, self::TRANSLATED_LAND );

		$this->assertCount( 1, IfOriginalPost::getTranslations( self::POST_ID ) );
		$translation = IfOriginalPost::getTranslations( self::POST_ID )[0];
		$this->assertEquals( false, $translation->original );
		$this->assertEquals( self::TRANSLATED_ID, $translation->element_id );
		$this->assertEquals( self::LANG, $translation->source_language_code );
		$this->assertEquals( self::TRANSLATED_LAND, $translation->language_code );
		$this->assertNotNull( $translation->trid );

		$this->assertCount( 1, IfOriginalPost::getTranslationIds( self::POST_ID ) );
		$this->assertEquals( wpml_collect( [ self::TRANSLATED_ID ] ), IfOriginalPost::getTranslationIds( self::POST_ID ) );
	}

	/**
	 * @test
	 */
	public function it_returns_callable_if_called_without_id() {
		PostTranslations::setAsTranslationOf( self::POST_ID, self::TRANSLATED_ID, self::TRANSLATED_LAND );

		$getTranslations = IfOriginalPost::getTranslations();
		$this->assertTrue( is_callable( $getTranslations ) );
		$this->assertCount( 1, $getTranslations( self::POST_ID ) );

		$getTranslationIds = IfOriginalPost::getTranslationIds();
		$this->assertTrue( is_callable( $getTranslationIds ) );
		$this->assertCount( 1, $getTranslationIds( self::POST_ID ) );
	}

}
