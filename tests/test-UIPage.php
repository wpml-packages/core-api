<?php

namespace WPML;

use tad\FunctionMocker\FunctionMocker;
use WPML\FP\Fns;
use WPML\FP\Logic;
use WPML\FP\Relation;

class Test_UIPage extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_confirms_pages() {
		$this->assertFalse( UIPage::isLanguages( [] ) );
		$this->assertTrue( UIPage::isLanguages( [ 'page' => WPML_PLUGIN_FOLDER . '/menu/languages.php' ] ) );

		$this->assertFalse( UIPage::isSettings( [] ) );
		$this->assertTrue( UIPage::isSettings( [ 'page' => WPML_TM_FOLDER . '/menu/settings' ] ) );
		$this->assertTrue( UIPage::isMainSettingsTab( [ 'page' => WPML_TM_FOLDER . '/menu/settings' ] ) );
		$this->assertTrue( UIPage::isMainSettingsTab( [ 'page' => WPML_TM_FOLDER . '/menu/settings', 'sm' => 'mcsetup' ] ) );
		$this->assertTrue( UIPage::isNotificationSettingsTab( [ 'page' => WPML_TM_FOLDER . '/menu/settings', 'sm' => 'notifications' ] ) );
		$this->assertTrue( UIPage::isCustomXMLConfigSettingsTab( [ 'page' => WPML_TM_FOLDER . '/menu/settings', 'sm' => 'custom-xml-config' ] ) );

		FunctionMocker::replace( 'defined', false );
		$this->assertTrue( UIPage::isSettings( [ 'page' => WPML_PLUGIN_FOLDER . '/menu/translation-options.php' ] ) );

		$this->assertFalse( UIPage::isTranslationManagement( [ 'page' => WPML_PLUGIN_FOLDER . '/menu/languages.php' ] ) );
		$this->assertTrue( UIPage::isTranslationManagement( [ 'page' => 'tm/menu/main.php' ] ) );

		$this->assertTrue( UIPage::isTMDashboard( [ 'page' => 'tm/menu/main.php' ] ) );
		$this->assertTrue( UIPage::isTMDashboard( [ 'page' => 'tm/menu/main.php', 'sm' => 'dashboard' ] ) );
		$this->assertFalse( UIPage::isTMDashboard( [ 'page' => 'tm/menu/main.php', 'sm' => 'aaa' ] ) );
		$this->assertFalse( UIPage::isTMDashboard( [
			'page' => WPML_PLUGIN_FOLDER . '/menu/translation-options.php',
			'sm'   => 'dashboard'
		] ) );

		$this->assertTrue( UIPage::isTMBasket( [ 'page' => 'tm/menu/main.php', 'sm' => 'basket' ] ) );
		$this->assertFalse( UIPage::isTMBasket( [ 'page' => 'tm/menu/main.php', 'sm' => 'aaa' ] ) );
		$this->assertFalse( UIPage::isTMBasket( [ 'page' => 'tm/menu/main.php' ] ) );
		$this->assertFalse( UIPage::isTMBasket( [
			'page' => WPML_PLUGIN_FOLDER . '/menu/translation-options.php',
			'sm'   => 'basket'
		] ) );

		$this->assertTrue( UIPage::isTMJobs( [ 'page' => 'tm/menu/main.php', 'sm' => 'jobs' ] ) );

		$this->assertTrue( UIPage::isTMTranslators( [ 'page' => 'tm/menu/main.php', 'sm' => 'translators' ] ) );

		$this->assertTrue( UIPage::isTMATE( [ 'page' => 'tm/menu/main.php', 'sm' => 'ate-ams' ] ) );

		$this->assertFalse( UIPage::isTroubleshooting( [] ) );
		$this->assertTrue( UIPage::isTroubleshooting( [ 'page' => WPML_PLUGIN_FOLDER . '/menu/troubleshooting.php' ] ) );

		$this->assertFalse( UIPage::isTranslationQueue( [] ) );
		$this->assertTrue( UIPage::isTranslationQueue( [ 'page' => 'tm/menu/translations-queue.php' ] ) );
	}

	/**
	 * @test
	 */
	public function it_gets() {
		$this->assertEquals( 'admin.php?page=' . WPML_PLUGIN_FOLDER . '/menu/languages.php', UIPage::getLanguages() );
		$this->assertEquals( 'admin.php?page=' . WPML_PLUGIN_FOLDER . '/menu/troubleshooting.php', UIPage::getTroubleshooting() );

		$this->assertEquals( 'admin.php?page=tm/menu/main.php', UIPage::getTM() );
		$this->assertEquals( 'admin.php?page=tm/menu/main.php&sm=dashboard', UIPage::getTMDashboard() );
		$this->assertEquals( 'admin.php?page=tm/menu/main.php&sm=basket', UIPage::getTMBasket() );
		$this->assertEquals( 'admin.php?page=tm/menu/main.php&sm=ate-ams', UIPage::getTMATE() );
		$this->assertEquals( 'admin.php?page=tm/menu/main.php&sm=translators', UIPage::getTMTranslators() );
		$this->assertEquals( 'admin.php?page=tm/menu/main.php&sm=jobs', UIPage::getTMJobs() );

		FunctionMocker::replace( 'defined', true );
		$this->assertEquals( 'admin.php?page=tm/menu/settings', UIPage::getSettings() );
		FunctionMocker::replace( 'defined', Logic::complement( Relation::equals( 'WPML_TM_FOLDER' ) ) );
		$this->assertEquals( 'admin.php?page=' . WPML_PLUGIN_FOLDER . '/menu/translation-options.php', UIPage::getSettings() );
	}


}
