<?php

namespace WPML\Element\API;

use WPML\API\PostMock;

class Test_Post extends \OTGS_TestCase {

	use PostMock;

	public function setUp() {
		parent::setUp();

		$this->setUpPostMock();
	}


	/**
	 * @test
	 */
	public function it_gets_post_original_language() {
		$postId       = 12;
		$expectedLang = 'fr';
		$this->setPostLang( $postId, $expectedLang );

		$this->assertEquals( $expectedLang, Post::getLang( $postId ) );
	}
}