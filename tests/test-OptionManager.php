<?php

namespace WPML\WP;

use WPML\LIB\WP\OptionMock;

class Test_OptionManager extends \OTGS_TestCase {

	use OptionMock;

	public function setUp() {
		parent::setUp();

		$this->setUpOptionMock();
	}
	/**
	 * @test
	 */
	public function it_sets_and_gets() {
		$optionManager = new OptionManager();

		$group = 'test';
		$key = 'abc';
		$value = [ 'some' => 'data' ];
		$default = 123;

		$optionManager->set( $group, $key, $value );

		$this->assertEquals( $value, $optionManager->get( $group, $key, $default ) );

		$this->assertEquals( $default, $optionManager->get( $group, 'wrong-key', $default ) );
	}

	/**
	 * @test
	 */
	public function it_updates_and_gets_with_static_fns() {
		$group = 'test';
		$key = 'abc';
		$value = [ 'some' => 'data' ];
		$default = 123;

		OptionManager::updateWithoutAutoLoad( $group, $key, $value );
		$this->assertEquals( $value, OptionManager::getOr( $default, $group, $key ) );
		$this->assertEquals( $default, OptionManager::getOr( $default, $group, 'wrong-key' ) );

		$key = 'xyz';

		OptionManager::update( $group, $key, $value );
		$this->assertEquals( $value, OptionManager::getOr( $default, $group, $key ) );
	}


}
